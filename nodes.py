'''
Created on Jul 18, 2014

@author: Matt
'''

import math, os
import utils
from PyQt4 import QtCore, QtGui
from PIL import Image, ImageOps, ImageQt

from GenericNodeGraph import *

import logging
  

class ODNodeScene(NodeScene):

    def __init__(self, parent=None):
        NodeScene.__init__(self, parent=parent)
        self.placementPos = None

    def dragEnterEvent(self, event):
        selectedNode = self.itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, ODNode):
            print 'DERP!'
        else:
            print selectedNode
            super(NodeScene, self).dragEnterEvent(event)

class ODNodeGraph(NodeGraph):
    """
    This is the main graph widget that will house the nodes
    """
    def __init__(self, parent=None):
        NodeGraph.__init__(self, parent=parent)

        self.timerId = 0

        self.mainScene = ODNodeScene(self)
        self.mainScene.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)
        self.sRect = QtCore.QRectF(-1280, -720, 2560, 1440)
        self.mainScene.setSceneRect(self.sRect)
        self.setScene(self.mainScene)
        self.setCacheMode(QtGui.QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QtGui.QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtGui.QGraphicsView.AnchorViewCenter)
        #drag around
        #self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)

        self.currentNode = None
        self.selectedNode = None

        self.nodeToConnectTo = None
        self.drawTempLine = False

        self.scale(0.8, 0.8)
        self.setMinimumSize(800, 400)


    def addNode(self, details, type):
        """
        add a contact node to the graph
        """
        newNode = None
        if type == "start":
            newNode = StartNode(self, details)
        elif type == "end":
            newNode = EndNode(self, details)
        elif type == "text":
            newNode = TextNode(self, details)
        elif type == "branch":
            newNode = BranchNode(self, details)
        elif type == "delay":
            newNode = DelayNode(self, details)
        elif type == "event":
            newNode = EventNode(self, details)

        self.mainScene.addItem(newNode)

        return newNode


    def mousePressEvent(self, event):
        self.update()

        #if event.buttons() == QtCore.Qt.LeftButton:
        #    print "lft"
        #elif event.buttons() == QtCore.Qt.RightButton:
        #    print "rght"
            #show instance of menu

        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        #if click out connection then set the node to connect to
        if isinstance(selectedNode, OutConnection):
            self.setDragMode(QtGui.QGraphicsView.NoDrag)
            self.nodeToConnectTo = selectedNode.parent
            #start temp line here
            self.drawTempLine = True
        else:
            self.nodeToConnectTo = None


        super(NodeGraph, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.update()
        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, ODNode):
            #remove any edges first
            selectedNode.setSelected(True)
            #print selectedNode.isSelected()
        super(NodeGraph, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        self.drawTempLine = False
        self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
      
        #update the size/bounds of the scene
        tw = 0
        th = 0
        for n in self.allNodes:
            tw += n.boundingRect().width()
            th += n.boundingRect().height()

        self.sRect.setX(-tw * 2)
        self.sRect.setY(-th * 3)
        self.sRect.setWidth(tw * 4)
        self.sRect.setHeight(th * 6)
        self.mainScene.setSceneRect(self.sRect)

        #print self.scene().itemAt(self.mapToScene(event.pos()))
        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, ODNode):
            self.selectedNode = selectedNode
            self.parent().parent().parent().displayNodeDetails()

        #if node is in connection then finish connection with node to connect to
        if isinstance(selectedNode, InConnection):
            #start in connection drawing
            if not self.nodeToConnectTo == None:
                con = self.nodeToConnectTo.outConnections[0]
                newEdge = Edge(con,
                                     selectedNode.parent.inputCon)
                selectedNode.parent.addEdge(newEdge)
                self.mainScene.addItem(newEdge)


        super(NodeGraph, self).mouseReleaseEvent(event)

    def timerEvent(self, event):
        nodess = [item for item in self.scene().items() if isinstance(item, ODNode)]

        itemsMoved = False
        for node in nodess:
            if node.advance():
                itemsMoved = True

        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0


    def render(self, painter, option, widget):
        """
        """
        if self.drawTempLine:
            print "drawing"
            line = QtCore.QLineF(self.nodeToConnectTo.outputCon.pos(), self.pos())
            painter.setPen(QtGui.QPen(QtCore.Qt.white,
                                      1,
                                      QtCore.Qt.SolidLine,
                                      QtCore.Qt.RoundCap,
                                      QtCore.Qt.RoundJoin))
            painter.drawLine(line)
            self.mainScene.addItem(line)



class ODNode(Node):
    """
    The basic node class
    """
    Type = QtGui.QGraphicsItem.UserType + 1
    

    def __init__(self, parent=None, details=None):
        Node.__init__(self, parent=parent, details=details)

        self.graph = parent
        self.nodeType = utils.enum('START',
                                   'TEXT',
                                   'BRANCH',
                                   'DELAY',
                                   'EVENT',
                                   'END')
        self.type = self.nodeType.START
        self.edgeList = []
        self.newPos = QtCore.QPointF()
        self.wh = (320,150)
        self.sz = {"halfw":self.wh[0]/2,
                   "halfh":self.wh[1]/2,
                   "fourw":self.wh[0]/4,
                   "fourh":self.wh[1]/4,
                   "fivew":self.wh[0]/5,
                   "fiveh":self.wh[1]/5,
                   "tenw":self.wh[0]/10,
                   "tenh":self.wh[1]/10}

        self.details = details
        #for k,v in details.iteritems():
        #    print k,v
        
        self.boundSize = QtCore.QRectF(0,-18,self.wh[0],self.wh[1]+30)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        self.rimColor = QtGui.QColor(55, 55, 55, alpha=200)
       

    def refreshMe(self):
        """
        refresh and update the details
        """
        if "portrait" in self.details:
            imgPth = self.details["portrait"]
            self.img = QtGui.QImage(imgPth)
        self.update()
    
    def shape(self):
        path = QtGui.QPainterPath()
        path.addRoundedRect(self.shapeSize.x(),
                            self.shapeSize.y(),
                            self.shapeSize.width(),
                            self.shapeSize.height(),
                            7,
                            7)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)

        #draw outline
        #painter.setBrush(QtGui.QBrush(gradient))
        #fillCol = QtGui.QColor()
        #fillCol.setRgb(34, 35, 38, alpha=125)
        #painter.setBrush(fillCol)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        #draw image
        imgwh = 64
        imgr = QtCore.QRect(self.sz["tenh"],
                            self.sz["tenh"],
                            imgwh,
                            imgwh)
        painter.drawImage(imgr, self.img)
        
        #draw name
        nrec = QtCore.QRect((self.sz["tenh"] *2) + imgwh,
                            self.sz["tenh"],
                            self.wh[0],
                            24)
        painter.drawText(nrec,
                         QtCore.Qt.AlignLeft,
                         "Who: {0}".format(self.details['who']) )
        
        #draw text
        opt = QtGui.QTextOption()
        opt.setAlignment(QtCore.Qt.AlignLeft)
        opt.setWrapMode(QtGui.QTextOption.WordWrap)
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        trec = QtCore.QRectF(self.sz["tenh"],
                           self.sz["tenh"] + imgr.height(),
                           self.wh[0] - self.sz["fivew"],
                           self.sz["halfh"])
        painter.drawText(trec,
                         self.details['text'],
                         option = opt)
        
        #draw response
        i=0
        totalRespH = 0;
        for con in self.outConnections:
            nrec = QtCore.QRect((self.sz["tenh"] *2) + imgwh,
                                trec.y() + trec.height() + totalRespH,
                                self.wh[0],
                                24)
            totalRespH += 24
            painter.drawText(nrec,
                             QtCore.Qt.AlignLeft,
                             "Response: {0}".format(self.details['responses'][i]) )
            i+=1
        
        
        sw = trec.width()
        if (nrec.width() + imgr.width()) > trec.width():
            sw = (nrec.width() + imgr.width())
        sh = imgr.height() + trec.height() + totalRespH
        self.shapeSize.setWidth(sw + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + self.sz["tenh"])
        self.shapeSize.setHeight(sh + self.sz["tenh"])
        self.boundSize.setHeight(self.shapeSize.height() + (self.sz["tenh"] * 2))
        

    def hoverEnterEvent(self, event):
        self.update()
        #set as current node
        self.graph.currentNode = self
        super(Node, self).hoverEnterEvent(event)

        
#================ NODEs =======================================================#

class StartNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        ODNode.__init__(self, parent=parent, details=details)
        
        self.type = self.nodeType.START
        self.wh = [125, 100]
        self.boundSize = QtCore.QRectF(0,-15,self.wh[0],self.wh[1]+15)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        
        self.rimColor = QtGui.QColor(200, 200, 200, alpha=255)
        
        #remove inConnection
        #self.group.removeFromGroup(self.inputCon)
        #self.graph.mainScene.removeItem(self.inputCon)
        #self.inputCon = None
        
    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        #draw name
        nrec = QtCore.QRect(self.sz["tenh"],
                            self.sz["tenh"],
                            self.wh[0],
                            24)
        painter.drawText(nrec,
                         QtCore.Qt.AlignLeft,
                         "Who: {0}".format(self.details['who']) )
        
        self.shapeSize.setWidth(nrec.width() + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + 5)
        
        
class EndNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        super(EndNode, self).__init__(parent=parent, details=details)
        
        self.type = self.nodeType.END
        self.wh = [100, 100]
        self.boundSize = QtCore.QRectF(0,-15,self.wh[0],self.wh[1]+15)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        self.rimColor = QtGui.QColor(153, 11, 11, alpha=255)
        
        #remove output connection
        #for oc in self.outConnections:
        #    self.group.removeFromGroup(oc)
        #    self.graph.mainScene.removeItem(oc)
        #    oc = None
        
    
    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        self.shapeSize.setWidth(trec.width() + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + 5)
        
        
class TextNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        super(TextNode, self).__init__(parent=parent, details=details)
        
        self.type = self.nodeType.TEXT
        self.rimColor = QtGui.QColor(6, 133, 135, alpha=255)
        
class BranchNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        super(BranchNode, self).__init__(parent=parent, details=details)
        
        self.type = self.nodeType.BRANCH
        self.rimColor = QtGui.QColor(252, 91, 63, alpha=255)
        
        
class DelayNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        super(DelayNode, self).__init__(parent=parent, details=details)
        
        self.type = self.nodeType.DELAY
        self.wh = [150, 150]
        self.boundSize = QtCore.QRectF(0,-15,self.wh[0],self.wh[1]+15)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        self.rimColor = QtGui.QColor(111, 176, 127, alpha=255)
        
    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        #draw time
        nrec = QtCore.QRect(self.sz["tenh"],
                            self.sz["tenh"],
                            self.wh[0],
                            24)
        painter.drawText(nrec,
                         QtCore.Qt.AlignLeft,
                         "Time: {0}".format(self.details['time']) )
        
        self.shapeSize.setWidth(nrec.width() + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + 5)
        
class EventNode(ODNode):
    
    def __init__(self, parent=None, details=None):
        super(EventNode, self).__init__(parent=parent, details=details)
        
        self.type = self.nodeType.EVENT
        self.wh = [150, 150]
        self.boundSize = QtCore.QRectF(0,-15,self.wh[0],self.wh[1]+15)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        self.rimColor = QtGui.QColor(252, 176, 60, alpha=255)
        
    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        #draw time
        nrec = QtCore.QRect(self.sz["tenh"],
                            self.sz["tenh"],
                            self.wh[0],
                            24)
        painter.drawText(nrec,
                         QtCore.Qt.AlignLeft,
                         "Method: {0}".format(self.details['method']) )
        
        self.shapeSize.setWidth(nrec.width() + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + self.sz["tenh"])
        
        


"""class CloseButton(SimpleButton):
    
    def __init__(self, parent=None, details=None):
        super(CloseButton, self).__init__()

        self.parent = parent
        self.graph = parent.graph
        self.fillColor = {"r":255, "g":55, "b":55, "a":200}
        self.graph.scene().addItem(self)
        
    def closeMe(self):
        Node.closeMe(self.parent)
        
    def mousePressEvent(self, event):
        self.update()
        super(Node, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.update()
        super(Node, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        #print event.pos()
        #print self.scene().itemAt(event.pos())
        super(Node, self).mouseReleaseEvent(event)

    def hoverEnterEvent(self, event):
        self.update()
        super(Node, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.update()
        super(Node, self).hoverLeaveEvent(event)"""
        



        
        
