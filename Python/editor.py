"""
Created on Jul 18, 2014

@author: Matt
"""

VERSION = 0.01

#----------------------------------------------------------------------------#
#----------------------------------------------------------------- IMPORTS --#
# Built-in
import sys, os, math, logging, random

# Third-Party

# UI related
from PyQt4 import QtGui,QtCore

#local
import utils, nodes


logging.basicConfig(level=logging.DEBUG)


#----------------------------------------------------------------------------#
#--------------------------------------------------------------- FUNCTIONS --#
def main():
    """
    This is where the main program execution begins.
    @return: None
    """
    app = QtGui.QApplication(sys.argv)
    app.setStyle('Plastique')

    widget = OpenDialogue()
    widget.show()

    sys.exit(app.exec_())


#----------------------------------------------------------------------------#
#----------------------------------------------------------------- CLASSES --#

class OpenDialogue(QtGui.QMainWindow):
    """This is my main window for my email client.
    """
    def __init__(self,parent=None):
        QtGui.QMainWindow.__init__(self, parent) # frameless window QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowSystemMenuHint
        #make whole app transparent self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        self.setGeometry(150,150,1280,720)
        self.setWindowTitle("Open Dialogue v{0}".format(VERSION))

        #try config file
        self.config = utils.UserConfig()

        #this will hold all ui elements throughout application
        self.uiWidgets = {}
        self.settingsWindow = None
        self.creationPos = None

        self.createActions()
        self.createMenu()

        self.CreateUI()

        #load styledata
        self.styleData = utils.get_stylesheet()
        self.uiWidgets['styledata'] = self.styleData
        self.setStyleSheet(self.styleData)

        #load the default or last used file
        self.loadFile(True)


    def loadFile(self, default=False):
        """
        load the last used dialogue file or get dialog load new
        """
        if default:
            self.data = utils.getDialogueData(self.config)
        else:
            fl = QtGui.QFileDialog.getOpenFileName(self,
                                               'Open Dialogue',
                                               self.config.settings["last_dir_opened"])
            if fl == '':
                return
            else:
                #load the first dialogue in the list
                self.config.settings["last_file_opened"] = str(fl)
                self.data = utils.getDialogueData(self.config)

        print self.data
        self.refreshDialogueView()

    def refreshDialogueView(self):
        """
        data should be loaded so now populate views
        """
        #populate the dialogues
        for d in self.data["dialogues"]:
            self.uiWidgets["dialogueList"].insertItem(self.uiWidgets["dialogueList"].count(), d["name"])

        #set default dialogue scene
        self.uiWidgets['dialogueList'].setCurrentRow(0)
        self.loadNewDialogue(self.uiWidgets['dialogueList'].item(0))


    def createMenu(self):
        """
        Create the main menu bar
        """
        self.uiWidgets['fileMenu'] = self.menuBar().addMenu("&File")
        self.uiWidgets['fileMenu'].addAction(self.loadAction)
        self.menuBar().addSeparator()
        self.uiWidgets['fileMenu'].addAction(self.exitAction)

        self.uiWidgets['aboutMenu'] = self.menuBar().addMenu("&Help")
        self.uiWidgets['aboutMenu'].addAction(self.aboutAction)

        
    def createContextMenu(self, nodeType=None):
        """
        create a custom right click-context menu
        """
        self.ccmenu = QtGui.QMenu("CCMenu", self)
        subMenu = QtGui.QMenu("Add Node", self)
        
        print nodeType
        
        #branch
        if nodeType == 2:
            self.addBranchOutputAction = QtGui.QAction("A&dd Output",
                                                       self,
                                                       shortcut="B",
                                                       statusTip="Add a output",
                                                       triggered=lambda : self.addNewOutput("branch"))
            self.ccmenu.addAction(self.addBranchOutputAction)
        
        subMenu.addAction(self.addStartNodeAction)
        subMenu.addAction(self.addTextNodeAction)
        subMenu.addAction(self.addBranchNodeAction)
        subMenu.addAction(self.addDelayNodeAction)
        subMenu.addAction(self.addEventNodeAction)
        subMenu.addAction(self.addEndNodeAction)

        self.ccmenu.addMenu(subMenu)
        self.ccmenu.addAction(self.removeNodeAction)


    def createActions(self):
        """
        create the actions that are tied to the menu bar
        """
        self.toFrontAction = QtGui.QAction(
            QtGui.QIcon(':/images/bringtofront.png'), "Bring to &Front",
            self, shortcut="Ctrl+F", statusTip="Bring item to front",
            triggered=self.bringToFront)

        self.loadAction =  QtGui.QAction("L&oad", self, shortcut="Ctrl+L",
                                         statusTip="Load File",
                                         triggered=self.loadFile)

        self.exitAction = QtGui.QAction("E&xit", self, shortcut="Ctrl+X",
                                        statusTip="Quit Scene",
                                        triggered=self.close)

        #add nodes actions

        self.addStartNodeAction = QtGui.QAction("A&dd Start Node", self, shortcut="S",
                                        statusTip="Add the basic start node",
                                        triggered=lambda : self.addNewNode("start"))

        self.addTextNodeAction = QtGui.QAction("A&dd Text Node", self, shortcut="T",
                                        statusTip="Add a basic text node",
                                        triggered=lambda : self.addNewNode("text"))

        self.addBranchNodeAction = QtGui.QAction("A&dd Branch Node", self, shortcut="B",
                                        statusTip="Add a branch node",
                                        triggered=lambda : self.addNewNode("branch"))

        self.addDelayNodeAction = QtGui.QAction("A&dd Delay Node", self, shortcut="D",
                                        statusTip="Add a delay node",
                                        triggered=lambda : self.addNewNode("delay"))

        self.addEventNodeAction = QtGui.QAction("A&dd Event Node", self, shortcut="V",
                                        statusTip="Add a event node",
                                        triggered=lambda : self.addNewNode("event"))

        self.addEndNodeAction = QtGui.QAction("A&dd End Node", self, shortcut="E",
                                        statusTip="Add a end node",
                                        triggered=lambda : self.addNewNode("end"))

        self.saveAction = QtGui.QAction("S&ave", self, shortcut="Ctrl+S",
                                        statusTip="Save the current dialogue scene",
                                        triggered=self.saveScene)

        #end add nodes actions

        self.aboutAction = QtGui.QAction("A&bout", self, shortcut="Ctrl+B",
                                         triggered=self.about)

        #context Menu actions
        self.removeNodeAction = QtGui.QAction("R&emove Node",
                                              self,
                                              shortcut="Ctrl+X",
                                              triggered=self.removeNode)

    def displayNodeDetails(self):
        """
        display a window taking the data from the current node
        and when the data is done being edited, save it
        """
        #print self.graph.currentNode.type

        #remove the current editor and replace with a new one based on the node
        self.leftColLayout.removeWidget(self.nEdit)
        self.nEdit.hide()
        self.nEdit = None

        self.nEdit = EditorPopUp(self.graph.currentNode.details)
        self.nEdit.saveBtn.clicked.connect(self.updateNodeDetails)

        self.leftColLayout.addWidget(self.nEdit)


    def updateNodeDetails(self):
        """
        get the value from the details editor and set the data
        on the node itself
        """
        if "who" in self.graph.currentNode.details:
            self.graph.currentNode.details["who"] = str(self.nEdit.nEdit.text())
        if "portrait" in self.graph.currentNode.details:
            self.graph.currentNode.details["portrait"] = str(self.nEdit.imgEdit.text())
        if "responses" in self.graph.currentNode.details:
            for i in range(len(self.graph.currentNode.details["responses"])):
                self.graph.currentNode.details["responses"][i] = str(self.nEdit.respEdits[i].text())
        if "method" in self.graph.currentNode.details:
            self.graph.currentNode.details["method"] = str(self.nEdit.methEdit.text())
            for i in range(len(self.graph.currentNode.details["methodArgs"])):
                self.graph.currentNode.details["methodargs"][i] = str(self.nEdit.methArgs[i].text())
        if "time" in self.graph.currentNode.details:
            self.graph.currentNode.details["time"] = int(self.nEdit.delayEdit.value())
        if "text" in self.graph.currentNode.details:
            self.graph.currentNode.details["text"] = str(self.nEdit.txtEdit.toPlainText())
        self.graph.currentNode.refreshMe()

    def removeNode(self):
        """
        get rid of the current node that is being hovered over
        """
        self.graph.currentNode.closeMe()


    def CreateUI(self):

        self.mWidget = QtGui.QWidget()
        self.mLayout = QtGui.QGridLayout()
        self.mWidget.setLayout(self.mLayout)
        self.setCentralWidget(self.mWidget)
        self.mLayout.setColumnMinimumWidth(0, 250)
        self.mLayout.setColumnMinimumWidth(1, 1000)

        self.CreateColOne()
        self.CreateColTwo()

        self.mLayout.addWidget(self.uiWidgets['leftCol'], 0,0)
        self.mLayout.addWidget(self.uiWidgets['rightCol'],0,1)
        self.show()

    def CreateColOne(self):
        """
        create the list of dialogues
        """
        self.uiWidgets['leftCol'] = QtGui.QWidget()
        self.leftColLayout = QtGui.QVBoxLayout()
        self.uiWidgets['leftCol'].setLayout(self.leftColLayout)

        dLbl = QtGui.QLabel("Dialogues")
        btnWig = QtGui.QWidget()
        btnLay = QtGui.QHBoxLayout()
        btnWig.setLayout(btnLay)
        self.uiWidgets['addDialogueBtn'] = QtGui.QPushButton("+")
        self.uiWidgets['removeDialogueBtn'] = QtGui.QPushButton("-")

        self.uiWidgets["dialogueList"] = QtGui.QListWidget()

        btnLay.addWidget(self.uiWidgets['addDialogueBtn'])
        btnLay.addWidget(self.uiWidgets['removeDialogueBtn'])

        #add click listener
        self.connect(self.uiWidgets['addDialogueBtn'],
                     QtCore.SIGNAL("clicked()"),
                     self.addNewDialogue)
        self.connect(self.uiWidgets['removeDialogueBtn'],
                     QtCore.SIGNAL("clicked()"),
                     self.removeDialogue)

        #click for list items
        self.uiWidgets['dialogueList'].itemClicked.connect(self.loadNewDialogue)

        #create area for editing node data
        self.nEdit = QtGui.QWidget()
        #self.nEdit.saveBtn.clicked.connect(self.updateNodeDetails)

        self.leftColLayout.addWidget(dLbl)
        self.leftColLayout.addWidget(btnWig)
        self.leftColLayout.addWidget(self.uiWidgets['dialogueList'])
        self.leftColLayout.addWidget(self.nEdit)

    def CreateColTwo(self):
        """
        crate the graphe and node control bar
        """
        self.uiWidgets['rightCol'] = QtGui.QWidget()
        rightColLayout = QtGui.QVBoxLayout()
        self.uiWidgets['rightCol'].setLayout(rightColLayout)
        self.graph = nodes.ODNodeGraph(self)
        self.graph.show()

        barWid = QtGui.QWidget()
        barWidLayout = QtGui.QHBoxLayout()
        barWid.setLayout(barWidLayout)

        nLbl = QtGui.QLabel("Dialogue Name: ")
        self.uiWidgets["dialogueName"] = QtGui.QTextEdit()
        self.uiWidgets["dialogueName"].textChanged.connect(self.updateDialogueName)

        self.toolbar = QtGui.QToolBar()
        self.toolbar.addAction(self.saveAction)

        barWidLayout.addWidget(nLbl)
        barWidLayout.addWidget(self.uiWidgets["dialogueName"])
        barWidLayout.addWidget(self.toolbar)

        rightColLayout.addWidget(barWid)
        rightColLayout.addWidget(self.graph)


    def addNewDialogue(self):
        """
        create a new dialogue in the list and scene
        """
        listLen = self.uiWidgets['dialogueList'].count()
        newDialogue = utils.defaultDataStructs("dialogue", {"listlen":listLen})
        #or use insertItem(ind, QListWidgetItem)
        self.uiWidgets['dialogueList'].addItems([newDialogue["name"]])
        self.data['dialogues'].append(newDialogue)
        self.uiWidgets["dialogueName"].setText(self.data["dialogues"][listLen]["name"])
        print self.data

    def removeDialogue(self):
        selected = self.uiWidgets['dialogueList'].currentItem()
        ind = self.uiWidgets['dialogueList'].currentRow()
        self.uiWidgets['dialogueList'].takeItem(ind)
        for d in self.data['dialogues']:
            if str(d["name"]) == str(selected.text()):
                del self.data['dialogues'][ind]
        print self.data

    def loadNewDialogue(self, item):
        """
        clear scene and load the selected dialogue nodes
        """
        #clear the scene
        self.graph.mainScene.clear()
        #get index of Selection
        ind = self.uiWidgets['dialogueList'].row(item)
        self.graph.createSceneNodes(self.data["dialogues"][ind]["tree"])
        self.uiWidgets["dialogueName"].setText(self.data["dialogues"][ind]["name"])

    def updateDialogueName(self):
        """
        get text from name input and set the data value and update the text
        in the selected list item
        """
        ind = self.uiWidgets["dialogueList"].currentRow()
        newTxt = self.uiWidgets["dialogueName"].toPlainText()
        self.data["dialogues"][ind]["name"] = str(newTxt)
        self.uiWidgets["dialogueList"].currentItem().setText(self.data["dialogues"][ind]["name"])

    def saveScene(self):
        """
        gather all the data for the nodes and save it
        """
        ind = self.uiWidgets["dialogueList"].currentRow()
        for child in self.graph.mainScene.children():
            if isinstance(child, nodes.Node):
                print("child is {0}".format(child))
                for node in self.data["dialogues"][ind]["tree"]:
                    print node
                    if node["id"] == child.id:
                        print "match"
                        node["position"] = [child.pos().x(), child.pos().y()]


        utils.setDialogueData(self.data)

    def addNewNode(self, type):
        """
        create a new node base on the type
        @param type: a str the represents the node type
        """
        #check the type and create new data based on that
        newDetails = utils.defaultDataStructs(type)
        print self.creationPos
        #set position to mouse position on graph
        newDetails["position"] = [self.creationPos.x(), self.creationPos.y()]

        ind = self.uiWidgets['dialogueList'].currentRow()
        self.data["dialogues"][ind]["tree"].append(newDetails)
        self.graph.addNode(newDetails, type)
        print "selected type to create is {0}".format(type)
        
    def addNewOutput(self, type):
        """
        create a new output for the given node
        """
        if type == "branch":
            self.graph.currentNode.addOutput(len(self.graph.currentNode.outConnections), True)

    def bringToFront(self):
        pass

    def about(self):
        """
        show a small pop-up dialog with info about author/check for updates
        """
        mb = utils.generic_msg("""Open Dialogue is a product of \n
        Atmos Interactive LLC. Please Use At Own RISK!""",
                               "info",
                               True)
        #mb.setStyleSheet(self.styleData)


    def closeEvent(self, event):
        """
        close the application
        """
        #close Imap connection
        event.accept()
        QtGui.QApplication.quit()




#----------------------------------------------------------------------------#
#----------------------------------------------------------------- CLASSES --#


class EditorPopUp(QtGui.QWidget):

    def __init__(self, details, parent=None):
        QtGui.QWidget.__init__(self, parent=parent)

        self.details = details

        lyt = QtGui.QVBoxLayout()
        self.setLayout(lyt)
        #print details
        frmW = QtGui.QWidget()
        frmLyt = QtGui.QFormLayout()
        frmW.setLayout(frmLyt)

        if "who" in self.details:
            self.nEdit = QtGui.QLineEdit(self.details["who"])
            frmLyt.addRow("&Name: ", self.nEdit)

        if "portrait" in self.details:
            self.imgEdit = QtGui.QLineEdit(self.details["portrait"])
            self.imgEditBtn = QtGui.QPushButton("...")
            self.imgEditBtn.clicked.connect(self.get_image)
            frmLyt.addRow(self.imgEdit, self.imgEditBtn)

        if "audio" in self.details:
            self.audioEdit = QtGui.QLineEdit(self.details["audio"][0])
            self.audioEditBtn = QtGui.QPushButton("...")
            self.audioEditBtn.clicked.connect(self.get_audio)
            frmLyt.addRow(self.audioEdit, self.audioEditBtn)

        if "responses" in self.details:
            self.respEdits = []
            for resp in self.details["responses"]:
                re = QtGui.QLineEdit(resp)
                self.respEdits.append(re)
                frmLyt.addRow("&Response:", re)

        if "method" in self.details:
            self.methEdit = QtGui.QLineEdit(self.details["method"])
            frmLyt.addRow("&Method:", self.methEdit)
            self.methArgs = []
            for (k,v) in self.details["methodargs"].iteritems():
                mae = QtGui.QLineEdit(v)
                self.methArgs.append(mae)
                frmLyt.addRow("&{0}".format(k), mae)
            

        if "time" in self.details:
            self.delayEdit = QtGui.QDoubleSpinBox()
            self.delayEdit.setMinimum(0)
            self.delayEdit.setMaximum(100)
            self.delayEdit.setValue(self.details["time"])
            frmLyt.addRow("&Delay:", self.delayEdit)

        lyt.addWidget(frmW)

        if "text" in self.details:
            txtEditLbl = QtGui.QLabel("Body Text: ")
            self.txtEdit = QtGui.QTextEdit(self.details["text"])
            lyt.addWidget(txtEditLbl)
            lyt.addWidget(self.txtEdit)

        #ok/cancel
        self.saveBtn = QtGui.QPushButton("OK")
        lyt.addWidget(self.saveBtn)

    def get_image(self):
        """
        show file dialog and update image path
        """
        fl = QtGui.QFileDialog.getOpenFileName(self,
                                               'Open Image',
                                               str(self.imgEdit.text()))
        if fl == '':
            return
        else:
            self.imgEdit.setText(fl)

    def get_audio(self):
        """
        show file dialoge to get audio file path
        """
        fl = QtGui.QFileDialog.getOpenFileName(self,
                                               'Open Audio File',
                                               str(self.audioEdit.text()))
        if fl == '':
            return
        else:
            self.audioEdit.setText(fl)





if __name__ == "__main__":
    main()
