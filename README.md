# README #

OpenDialogue

### What is this repository for? ###

* OpenDialogue, an open source node based dialogue tree building tool with and extensible Unity addition.
* Version v0.0.2


### How do I get set up? ###

* Summary of set up: Install Python and PySide
* Configuration
* Dependencies: Python, PySide, Unity


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Matt Murray [@mattanimation] - mattanimation@gmail.com
