﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;


public class DialogueNode
{
	public const float kNodeSize = 150.0f;
	
	public static DialogueNode selection = null;
	public static bool connecting = false;



	public Rect window1, window2, _handleArea, _handleAreaScaled;
	
	private bool _refreshLinks, _nodeOption, _handleActive, _options;
	
	private Texture2D _resizeHandle;
	private GUIContent _icon;
	private float _winMinX, _winMinY, _deltaScale, _offset;


	
	public Vector2 position;
	private Rect nodeRect;
	private Rect textRect;
	private Rect closeBtnRect;
	public string name;
	public List<DialogueNode> targets = new List<DialogueNode> ();

	//private float _deltaScale = 1.0f;
	private float halfkNode = 0f;
	private float twlv = 0f;

	public string nodeText = "";



	public DialogueNode (string name, Vector2 position)
	{
		this.name = name;
		Position = position;
	}
	
	
	public static DialogueNode Selection
	{
		get
		{
			return selection;
		}
		set
		{
			selection = value;
			if (selection == null)
			{
				connecting = false;
			}
		}
	}
	
	
	public Vector2 Position
	{
		get
		{
			return position;
		}
		set
		{
			position = value;

			halfkNode = kNodeSize * 0.5f;
			twlv = kNodeSize/12;
			nodeRect = new Rect (
				position.x - halfkNode,
				position.y - halfkNode,
				kNodeSize,
				kNodeSize
				);
			textRect = new Rect (
				position.x - halfkNode + twlv,
				position.y - halfkNode + (twlv*2),
				kNodeSize-(twlv*2),
				kNodeSize/2
				);
			closeBtnRect = new Rect(
				position.x + halfkNode,
				position.y - halfkNode,
				25,
				25
				);
		}
	}
	
	
	public ReadOnlyCollection<DialogueNode> Targets
	{
		get
		{
			return targets.AsReadOnly ();
		}
	}
	
	
	public void ConnectTo (DialogueNode target)
	{
		if (targets.Contains (target))
		{
			return;
		}
		
		targets.Add (target);
	}
	
	
	public void OnGUI ()
	{

		switch (Event.current.type)
		{
		case EventType.mouseDown:
			// Select this node if we clicked it
			if (nodeRect.Contains (Event.current.mousePosition))
			{
				selection = this;
				
				if (Event.current.clickCount == 2)
					// If we double-clicked it, enter connect mode
				{
					connecting = true;
				}
				
				Event.current.Use ();
			}

			break;
		case EventType.mouseUp:
			// If we released the mouse button...
			if (selection == null)
				// ... with no active selection, ignore the event
			{
				break;
			}
			else if (selection == this)
				// ... while this node was active selection...
			{
				if (!connecting)
					// ... and we were not in connect mode, clear the selection
				{
					Selection = null;
					Event.current.Use ();
				}
			}
			else if (connecting && nodeRect.Contains (Event.current.mousePosition))
				// ... over this component while in connect mode, connect selection to this node and clear selection
			{
				selection.ConnectTo (this);
				Selection = null;
				Event.current.Use ();
			}
			break;
		case EventType.mouseDrag:
			if (selection == this)
				// If doing a mouse drag with this component selected...
			{
				if (connecting)
					// ... and in connect mode, just use the event as we'll be painting the new connection
				{
					Event.current.Use ();
				}
				else
					// ... and not in connect mode, drag the component
				{
					Position += Event.current.delta;
					Event.current.Use ();
				}
			}
			break;
		case EventType.repaint:
			// The component box
			GUI.skin.box.Draw (nodeRect, new GUIContent (name), false, false, false, false);


			// The new connection
			if (selection == this && connecting)
			{
				GUI.color = Color.red;
				DrawConnection (position, Event.current.mousePosition);
				GUI.color = Color.white;
			}
			break;
		}
		nodeText = GUI.TextArea(textRect, nodeText);
		if(GUI.Button(closeBtnRect,"X"))
		{
			DialogueEditor.closeNode(this);
		}


	

	}


	// Render a node connection between the two given points
	public static void DrawConnection (Vector2 from, Vector2 to)
	{
		bool left = from.x > to.x;
		
		Handles.DrawBezier(
			new Vector3 (from.x + (left ? -kNodeSize : kNodeSize) * 0.5f, from.y, 0.0f),
			new Vector3 (to.x + (left ? kNodeSize : -kNodeSize) * 0.5f, to.y, 0.0f),
			new Vector3 (from.x, from.y, 0.0f) + Vector3.right * 50.0f * (left ? -1.0f : 1.0f),
			new Vector3 (to.x, to.y, 0.0f) + Vector3.right * 50.0f * (left ? 1.0f : -1.0f),
			GUI.color,
			null,
			2.0f
		);
	}
}