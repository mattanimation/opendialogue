﻿using UnityEngine;
using System.Collections;

public class Achievement {

	private string mName;
	private string[] mProps;
	private bool mUnlocked;

	public bool Unlocked{
		get{return mUnlocked;}
		set{mUnlocked = value;}
	}

	public string[] Props{
		get{return mProps;}
		set{mProps = value;}
	}

	public Achievement(string theId, string[] theRelatedProps)
	{
		mName = theId;
		mProps = theRelatedProps;
		mUnlocked = false;
	}
}
