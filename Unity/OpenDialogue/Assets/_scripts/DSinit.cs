﻿using UnityEngine;
using System.Collections;

public class DSinit : MonoBehaviour {

	/*
	 * this script is used as example to show how to init the dialogue system
	 * */

	void Awake()
	{
		//create the one instance to be used for the game and or scene
		//OpenDialogue.Initialize();
	}

	void OnGUI()
	{
		//create  abutton to start a dialoge
		//if(GUI.Button(new Rect(10,10, 100, 35), "start Dialogue System"))
		//{
			//DialogueSystem.StartDialogue(0, InitCallback);
			//this.enabled = false;
		//}
	}

	bool InitCallback()
	{
		Debug.Log("init callback ran ");
		this.enabled = true;
		return true;
	}
}
