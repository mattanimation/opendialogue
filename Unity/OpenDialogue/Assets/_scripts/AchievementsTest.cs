﻿using UnityEngine;
using System.Collections;

public class AchievementsTest : MonoBehaviour {

	public Achieve a;


	public bool talkedToSomeone = false;
	public bool justDied = false;

	// Use this for initialization
	void Start () {

		a = new Achieve();
		a.DefineProperty("peopleTalkedTo",0,Achieve.ACTIVE_IF_GREATER_THAN, 2, "levelStuff");
		a.DefineProperty("lives", 3, Achieve.ACTIVE_IF_EQUAL_TO, 3, "levelStuff");
		a.DefineProperty("completedLevels", 0, Achieve.ACTIVE_IF_GREATER_THAN, 5);
		a.DefineProperty("deaths", 0, Achieve.ACTIVE_IF_EQUAL_TO, 0);
	

		string[] p = new string[1];
		p[0] = "peopleTalkedTo";
		string[] l = new string[1];
		l[0] = "lives";
		string[] cl = new string[1];
		cl[0] = "completedLevels";
		string[] cd = new string[2];
		cd[0] = "completedLevels";
		cd[1] = "deaths";
		a.DefineAchievement("chatterBox", p);
		a.DefineAchievement("cantTouchThis", l); // Complete a level and don't die.
		a.DefineAchievement("nothingElse",cl); // Beat all 5 levels.
		a.DefineAchievement("hero", cd); // Beat all 5 levels, do not die during the process

	}
	
	// Update is called once per frame
	void Update () {
	
		if(talkedToSomeone)
		{
			string[] s = new string[1];
			s[0] = "peopleTalkedTo";
			a.AddValue(s, 1);
		}

		if(justDied)
		{
			string[] l = new string[1];
			l[0] = "lives";
			string[] d = new string[1];
			d[0] = "deaths";
			a.AddValue(l, -1);
			a.AddValue(d, 1);
		}

	}

	void LevelUp()
	{
		string[] cl = new string[2];
		cl[0] = "completedLevels";
		a.AddValue(cl, 1);
		a.CheckAchievements();
		a.ResetProperties("levelStuff");
	}
}
