﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Achieve {

	//Activation rules
	public const string ACTIVE_IF_GREATER_THAN = ">";
	public const string ACTIVE_IF_LESS_THAN = "<";
	public const string ACTIVE_IF_EQUAL_TO = "==";

	private Dictionary<string, Property> mProps;
	private Dictionary<string, Achievement> mAchievements;

	public Achieve()
	{
		mProps = new Dictionary<string, Property>();
		mAchievements = new Dictionary<string, Achievement>();
	}

	public void DefineProperty(string theName, int theInitialValue, string theActivationMode, int theValue)
	{
		mProps.Add(theName, new Property(theName, theInitialValue, theActivationMode, theValue));
	}

	public void DefineProperty(string theName, int theInitialValue, string theActivationMode, int theValue, string theTag)
	{
		mProps.Add(theName, new Property(theName, theInitialValue, theActivationMode, theValue, theTag));
	}

	public void DefineAchievement(string theName, string[] theRelatedProps)
	{
		mAchievements.Add(theName, new Achievement(theName, theRelatedProps));
	}

	public int GetValue(string theProp)
	{
		return mProps[theProp].Value;
	}

	public void SetValue(string theProp, int theValue)
	{
		//only update if new value is gt or lt the current depending on what it needs to be
		switch(mProps[theProp].Activation)
		{
		case Achieve.ACTIVE_IF_GREATER_THAN: 
			theValue = theValue > mProps[theProp].Value ? theValue : mProps[theProp].Value; 
			break;
		case Achieve.ACTIVE_IF_LESS_THAN:
			theValue = theValue < mProps[theProp].Value ? theValue : mProps[theProp].Value;
			break;
		}
		mProps[theProp].Value = theValue;
	}

	public void AddValue(string[] theProps, int theValue)
	{
		Debug.Log ("value added");
		// increment a list of properties by same value
		for(int i=0; i < theProps.Length; i++)
		{
			SetValue(mProps[theProps[i]].Name, GetValue(theProps[i]) + theValue);
		}
	}

	public List<Achievement> CheckAchievements()
	{
		Debug.Log("checking for achievements!");
		List<Achievement> aRet = new List<Achievement>();
		foreach(string n in mAchievements.Keys)
		{
			Achievement a = mAchievements[n];
			if(!a.Unlocked){
				int activeProps = 0;
				for(int i=0; i < a.Props.Length; i++)
				{
					Property p = mProps[a.Props[i]];
					if(p.IsActive())
						activeProps++;
				}

				if(activeProps == a.Props.Length)
				{
					Debug.Log("GOT ONE!");
					a.Unlocked = true;
					aRet.Add(a);
				}
				
			}
		}
		return aRet;

	}

	public void ResetProperties(string theTag)
	{
		Debug.Log ("reset props here");
	}

}
