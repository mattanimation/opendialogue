﻿using UnityEngine;
using System.Collections;

public class FauxCharacter : MonoBehaviour {

	public string name = "george";

	public bool isClose = false;

	public bool justDied = false;
	
	public bool talkedToSomeone = false;

	// Use this for initialization
	/*void Start () {

		StartCoroutine(CheckDist());
	}

	public IEnumerator CheckDist()
	{
		yield return new WaitForSeconds(0.25f);

		Debug.Log("checking dist");
		GameObject plyr = GameObject.FindGameObjectWithTag("Player");
		if(Vector3.Distance(plyr.transform.position, transform.position) <= 2)
			isClose = true;
		else
			isClose = false;
	}*/


	void FixedUpdate()
	{
		GameObject plyr = GameObject.FindGameObjectWithTag("Player");
		if(Vector3.Distance(plyr.transform.position, transform.position) <= 2)
			isClose = true;
		else
			isClose = false;
	}

	void OnGUI()
	{
		if(isClose && !OpenDialogue.isConversing)
		{
			if(GUI.Button(new Rect(Screen.width/2 -75, Screen.height/2 - 15, 150, 35), "Start Conversation with " + name))
			{
				OpenDialogue.StartDialogue(name);
				talkedToSomeone = true;
				justDied = true;
			}
		}
	}
	

}
