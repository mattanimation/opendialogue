﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Text;

public class NodeEditorWindow : EditorWindow
{
	
	static NodeEditorWindow window;
	
	public Rect window1, window2, _handleArea, _handleAreaScaled;
	
	private bool _refreshLinks, _nodeOption, _handleActive, _options;
	private Texture2D _resizeHandle;
	private GUIContent _icon;
	private float _winMinX, _winMinY, _deltaScale, _offset;
	
	
	[MenuItem("Window/Node Editor")]
	static void Init()
	{
		window = (NodeEditorWindow)EditorWindow.GetWindow(typeof(NodeEditorWindow));
		window.title = "Node Editor";
		window.ShowNodes();
	}
	
	private void ShowNodes()
	{
		_winMinX = 100f;
		_winMinY = 100f;
		window1 = new Rect(30, 30, _winMinX, _winMinY);
		window2 = new Rect(210, 210, _winMinX, _winMinY);
	}
	
	void OnEnable()
	{
		_resizeHandle = AssetDatabase.LoadAssetAtPath("Assets/NodeEditor/Icons/ResizeHandle.png", typeof(Texture2D)) as Texture2D;
		_icon = new GUIContent(_resizeHandle);
	}
	
	void OnGUI()
	{
		_deltaScale = 1.0f;
		BeginWindows();
		window1 = GUI.Window(1, window1, DrawNodeWindow, "Window 1");   // Updates the Rect's when these are dragged
		window2 = GUI.Window(2, window2, DrawNodeWindow, "Window 2");
		EndWindows();
		if (_refreshLinks)
		{
			DrawNodeCurve(window1, window2);
		}
		GUILayout.BeginHorizontal(EditorStyles.toolbar);
		_options = GUILayout.Toggle(_options, "Toggle Me", EditorStyles.toolbarButton);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
	}
	
	private void DrawNodeWindow(int id)
	{
		float _cornerX = 0f;
		float _cornerY = 0f;
		switch (id)
		{
		case 1:
			_cornerX = window1.width;
			_cornerY = window1.height;
			break;
		case 2:
			_cornerX = window2.width;
			_cornerY = window2.height;
			break;
		}
		
		GUILayout.BeginArea(new Rect(1, 16, _cornerX - 3, _cornerY - 1));
		GUILayout.BeginHorizontal(EditorStyles.toolbar);
		_nodeOption = GUILayout.Toggle(_nodeOption, "Node Toggle", EditorStyles.toolbarButton);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
		GUILayout.BeginHorizontal(EditorStyles.toolbarTextField);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		_handleArea = GUILayoutUtility.GetRect(_icon, GUIStyle.none);
		_offset = Mathf.Abs(20 * _deltaScale);
		_handleAreaScaled = new Rect(_handleArea.xMin - _offset, _handleArea.yMin - _offset, 40 + _offset, 40 + _offset);
		
		if (_handleAreaScaled.Contains(Event.current.mousePosition, true))
		{
			_handleActive = true;
		}
		
		if ((_handleActive) && (Event.current.type == EventType.MouseDrag))
		{
			ResizeNode(id, Event.current.delta.x, Event.current.delta.y);
			Repaint();
			Event.current.Use();
		}
		else
		{
			_handleActive = false;
		}
		
		GUI.DrawTexture(new Rect(_handleArea.xMin + 3, _handleArea.yMin - 3, 20, 20), _resizeHandle);
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		if (!_handleActive)
		{
			_refreshLinks = true;
			GUI.DragWindow();
		}
	}
	
	private void ResizeNode(int id, float deltaX, float deltaY)
	{
		switch (id)
		{
		case 1:
			if ((window1.width + deltaX) > _winMinX) { window1.xMax += deltaX; }
			if ((window1.height + deltaY) > _winMinY) { window1.yMax += deltaY; }
			break;
		case 2:
			if ((window2.width + deltaX) > _winMinX) { window2.xMax += deltaX; }
			if ((window2.height + deltaY) > _winMinY) { window2.yMax += deltaY; }
			break;
		}
		_deltaScale = (deltaX + deltaY) / 2.0f;
	}
	
	void DrawNodeCurve(Rect start, Rect end)
	{
		Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
		Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
		Vector3 startTan = startPos + Vector3.right * 50;
		Vector3 endTan = endPos + Vector3.left * 50;
		Color shadowCol = new Color(0, 0, 0, 0.06f);
		for (int i = 0; i < 3; i++) // Draw a shadow
			Handles.DrawBezier(startPos, endPos, startTan, endTan, shadowCol, null, (i + 1) * 5);
		Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 1);
		_refreshLinks = false;
	}
	
}