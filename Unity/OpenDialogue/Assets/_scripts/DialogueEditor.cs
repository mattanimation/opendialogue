﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Text;

public class DialogueEditor : EditorWindow
{

	public enum NodeType{
		START,
		TEXT,
		BRANCH,
		END
	};

	static DialogueEditor window;

	public static List<DialogueNode> nodes = new List<DialogueNode>();

	public Rect window1, window2, _handleArea, _handleAreaScaled;
	
	private bool _refreshLinks, _nodeOption, _handleActive, _options;

	private Texture2D _resizeHandle;
	private GUIContent _icon;
	private float _winMinX, _winMinY, _deltaScale, _offset;

	private bool createTextNode = false;
	private string _newName ="";
	private bool createNewDialogue = false;

	private int toolbarInt = 0;
	private string[] toolbarStrings = {"tool 1", "tool 2", "tool 3"};

	public delegate void OnNodeClose(DialogueNode node);
	public static event OnNodeClose onNodeClose;

	
	[MenuItem("Dialogue System/Node Editor")]
	static void Init()
	{
		window = (DialogueEditor)EditorWindow.GetWindow(typeof(DialogueEditor));
		window.title = "Dialogue System Node Editor";
		window.ShowNodes();

		DialogueEditor.onNodeClose += closeNode;
	}
	
	private void ShowNodes()
	{
		_winMinX = 100f;
		_winMinY = 100f;
		window1 = new Rect(30, 30, _winMinX, _winMinY);
		window2 = new Rect(210, 210, _winMinX, _winMinY);

	}

	public static void closeNode(DialogueNode node)
	{
		Debug.Log(nodes.Count);
		Debug.Log(nodes.Remove(node));
		//node = null;
		Debug.Log(nodes);
	}

	void OnEnable()
	{
		_resizeHandle = AssetDatabase.LoadAssetAtPath("Assets/NodeEditor/Icons/ResizeHandle.png", typeof(Texture2D)) as Texture2D;
		_icon = new GUIContent(_resizeHandle);
	}
	
	void OnGUI()
	{
		//=====================================
		if (Event.current.type == EventType.repaint)
		{
			if(nodes.Count > 0 && nodes != null)
			{
				foreach (DialogueNode node in nodes)
				{
					if(node != null)
					{
						foreach (DialogueNode target in node.Targets)
						{
							DialogueNode.DrawConnection (node.Position, target.Position);
						}
					}
				}
			}
		}
		
		GUI.changed = false;
		
		// Handle all nodes
		if(nodes.Count > 0 && nodes != null)
		{
			foreach (DialogueNode node in nodes)
			{
				if(node != null)
					node.OnGUI();
			}
		}
		
		wantsMouseMove = DialogueNode.Selection != null;
		
		
		// If we have a selection, we're doing an operation which requires an update each mouse move
		switch (Event.current.type)
		{
		case EventType.mouseUp:
			// If we had a mouse up event which was not handled by the nodes, clear our selection
			Node.Selection = null;
			Event.current.Use ();
			break;
		case EventType.mouseDown:
			// If we double-click and no node handles the event, create a new node there
			if (Event.current.clickCount == 2)
			{
				CreateNewNode(NodeType.TEXT);
			}
			break;
		}
		
		// Repaint if we changed anything
		if (GUI.changed)
		{
			Repaint ();
		}

		//=====================================
		_deltaScale = 1.0f;
		BeginWindows();
		window1 = GUI.Window(1, window1, DrawNodeWindow, "Window 1");   // Updates the Rect's when these are dragged
		window2 = GUI.Window(2, window2, DrawNodeWindow, "Window 2");
		EndWindows();
		if (_refreshLinks)
		{
			DrawNodeCurve(window1, window2);
		}

		//create 2 main columns

		//col 1 holds list of dialogues
		GUILayout.BeginArea(new Rect(0,0, 250, 400));
		GUILayout.BeginHorizontal();
		GUILayout.BeginVertical();

			GUILayout.BeginHorizontal();
				GUILayout.Label("New Name");
				_newName = GUI.TextField(new Rect(0,0, 100, 30), _newName);
				if(GUILayout.Button("Add"))
				{
					Debug.Log(_newName);
					
				}
			GUILayout.EndHorizontal();

			GUILayout.BeginScrollView(Vector2.zero, GUIStyle.none);
				for(int i=0; i < 3; i++)
				{
					if(GUILayout.Button("Dialogue "+ i.ToString()))
					{
						GraphSelectedDialogue(i);
					}
				}
			GUILayout.EndScrollView();

		GUILayout.EndVertical();


		GUILayout.EndArea();

		//col 2 holds buttons to create nodes and nodes
		GUILayout.BeginArea(new Rect(250,0, 400, 400));
		GUILayout.BeginVertical();
		
		//menu bar buttons
		GUILayout.BeginHorizontal(EditorStyles.toolbar);
		_options = GUILayout.Toggle(_options, "Toggle Me", EditorStyles.toolbarButton);
		toolbarInt = GUI.Toolbar (new Rect (25, 25, 250, 30), toolbarInt, toolbarStrings);

		switch(toolbarInt)
		{
		case 0:
			Debug.Log("zerp");
			break;
		case 1:
			Debug.Log("derp");
			break;
		}

		//check button events
		if(GUI.Button(new Rect(0,0, 100, 35),"Text Node"))
		{
			Debug.Log ("!!!!");
			CreateNewNode(NodeType.TEXT);
		}
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		
		GUILayout.EndVertical();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();


	}

	public void GraphSelectedDialogue(int id)
	{
		Debug.Log ("should clear graph and load "+ id);
	}

	public void CreateNewNode(NodeType type)
	{
		Debug.Log("creating a new node of type: "+ type);

		switch(type)
		{
		case NodeType.START:
			break;

		case NodeType.TEXT:
			DialogueNode.Selection = new DialogueNode ("Node " + nodes.Count, Event.current.mousePosition);
			nodes.Add (DialogueNode.Selection);
			Event.current.Use();
			break;

		case NodeType.BRANCH:
			break;

		case NodeType.END:
			break;
		}
	}
	
	private void DrawNodeWindow(int id)
	{
		float _cornerX = 0f;
		float _cornerY = 0f;
		switch (id)
		{
		case 1:
			_cornerX = window1.width;
			_cornerY = window1.height;
			break;
		case 2:
			_cornerX = window2.width;
			_cornerY = window2.height;
			break;
		}
		
		GUILayout.BeginArea(new Rect(1, 16, _cornerX - 3, _cornerY - 1));
		GUILayout.BeginHorizontal(EditorStyles.toolbar);
		_nodeOption = GUILayout.Toggle(_nodeOption, "Node Toggle", EditorStyles.toolbarButton);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
		GUILayout.BeginHorizontal(EditorStyles.toolbarTextField);
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		GUILayout.BeginArea(new Rect(1, _cornerY - 16, _cornerX - 3, 14));
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		
		_handleArea = GUILayoutUtility.GetRect(_icon, GUIStyle.none);
		_offset = Mathf.Abs(20 * _deltaScale);
		_handleAreaScaled = new Rect(_handleArea.xMin - _offset, _handleArea.yMin - _offset, 40 + _offset, 40 + _offset);
		
		if (_handleAreaScaled.Contains(Event.current.mousePosition, true))
		{
			_handleActive = true;
		}
		
		if ((_handleActive) && (Event.current.type == EventType.MouseDrag))
		{
			ResizeNode(id, Event.current.delta.x, Event.current.delta.y);
			Repaint();
			Event.current.Use();
		}
		else
		{
			_handleActive = false;
		}
		
		GUI.DrawTexture(new Rect(_handleArea.xMin + 3, _handleArea.yMin - 3, 20, 20), _resizeHandle);
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
		
		if (!_handleActive)
		{
			_refreshLinks = true;
			GUI.DragWindow();
		}
	}
	
	private void ResizeNode(int id, float deltaX, float deltaY)
	{
		switch (id)
		{
		case 1:
			if ((window1.width + deltaX) > _winMinX) { window1.xMax += deltaX; }
			if ((window1.height + deltaY) > _winMinY) { window1.yMax += deltaY; }
			break;
		case 2:
			if ((window2.width + deltaX) > _winMinX) { window2.xMax += deltaX; }
			if ((window2.height + deltaY) > _winMinY) { window2.yMax += deltaY; }
			break;
		}
		_deltaScale = (deltaX + deltaY) / 2.0f;
	}
	
	void DrawNodeCurve(Rect start, Rect end)
	{
		Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
		Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
		Vector3 startTan = startPos + Vector3.right * 50;
		Vector3 endTan = endPos + Vector3.left * 50;

		Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 2);
		_refreshLinks = false;
	}
	
}