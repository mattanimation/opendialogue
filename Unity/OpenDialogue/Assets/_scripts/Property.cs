﻿using UnityEngine;
using System.Collections;

public class Property {

	private string mName;
	private int mValue;
	private string mActivation;
	private int mActivationValue;
	private int mInitialValue;
	private string mTag;


	public int Value{
		get{return mValue;}
		set{mValue = value;}
	}

	public string Name{
		get{return mName;}
		set{mName = value;}
	}

	public string Activation{
		get{return mActivation;}
		set{mActivation = value;}
	}

	public string Tag{
		get{return mTag;}
		set{mTag = value;}
	}

	public Property(string theName, int initialValue, string theActivation, int theActivationValue)
	{
		mName = theName;
		mInitialValue = initialValue;
		mActivation = theActivation;
		mActivationValue = theActivationValue;
		mTag = "";

	}

	public Property(string theName, int initialValue, string theActivation, int theActivationValue, string theTag)
	{
		mName = theName;
		mInitialValue = initialValue;
		mActivation = theActivation;
		mActivationValue = theActivationValue;
		mTag = theTag;
		
	}

	public bool IsActive()
	{
		bool aRet = false;
		switch(mActivation)
		{
		case Achieve.ACTIVE_IF_GREATER_THAN : aRet = mValue > mActivationValue; break;
		case Achieve.ACTIVE_IF_LESS_THAN : aRet = mValue < mActivationValue; break;
		case Achieve.ACTIVE_IF_EQUAL_TO : aRet = mValue == mActivationValue; break;
		}
		return aRet;
	}
}
