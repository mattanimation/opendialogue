'''
Created on Aug 31, 2014

@author: Matt
'''
#----------------------------------------------------------------------------#
#----------------------------------------------------------------- IMPORTS --#
# Built-in
import math, logging

# Third-Party

# UI related
from PyQt4 import QtGui,QtCore

#local
import utils


logging.basicConfig(level=logging.DEBUG)

#======================================= CLASSSES ============================#

class NodeScene(QtGui.QGraphicsScene):

    def __init__(self, parent=None):
        QtGui.QGraphicsScene.__init__(self, parent=parent)
        self.placementPos = None

    def mouseReleaseEvent(self, event):
        self.placementPos = event.scenePos()
        super(NodeScene, self).mouseReleaseEvent(event)

    def dragEnterEvent(self, event):
        selectedNode = self.itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, Node):
            print 'DERP!'
        else:
            print selectedNode
            super(NodeScene, self).dragEnterEvent(event)

class NodeGraph(QtGui.QGraphicsView):
    """
    This is the main graph widget that will house the nodes
    """
    def __init__(self, parent=None):
        QtGui.QGraphicsView.__init__(self, parent=parent)

        self.timerId = 0

        self.mainScene = NodeScene(self)
        self.mainScene.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)
        self.sRect = QtCore.QRectF(-1280, -720, 2560, 1440)
        self.mainScene.setSceneRect(self.sRect)
        self.setScene(self.mainScene)
        self.setCacheMode(QtGui.QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QtGui.QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtGui.QGraphicsView.AnchorViewCenter)
        #drag around
        #self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)

        self.currentNode = None
        self.selectedNode = None

        self.nodeToConnectTo = None
        self.drawTempLine = False

        self.scale(0.8, 0.8)
        self.setMinimumSize(800, 400)

    def dragMoveEvent(self,event):
        print "SDASs"

    def contextMenuEvent(self, event):
        """
        show the menu to edit the selected node
        """

        #print self.currentNode
        # Show the context menu.
        self.parent().parent().parent().creationPos = self.mainScene.placementPos
        #print event.pos()
        #print event.globalPos()
        #print self.mainScene.placementPos 
        self.parent().parent().parent().createContextMenu(self.currentNode.type)
        self.parent().parent().parent().ccmenu.exec_(event.globalPos())

    def createSceneNodes(self, dialogueNodes):
        """
        @param dialogueNodes - list of dicts - all nodes dat blocks
        """

        #create nodes by number of dialogue nodes
        self.allNodes = []
        for d in dialogueNodes:
            #print d["type"]
            self.allNodes.append(self.addNode(d, d["type"]))

        #loop over nodes and connect them up
        for node in self.allNodes:
            for oc in node.outConnections:
                #if not end node
                if not node.type == 5:
                    conNode = utils.get_node_by_id(self.allNodes, oc.id)
                    newEdge = Edge(oc,
                                 conNode.inputCon)
                    node.addEdge(newEdge)
                    self.mainScene.addItem(newEdge)


    def itemMoved(self):
        if not self.timerId:
            self.timerId = self.startTimer(1000 / 25)

    def addNode(self, details, type):
        """
        add a contact node to the graph
        """
        newNode = Node(self, details)
        self.mainScene.addItem(newNode)

        return newNode


    def keyPressEvent(self, event):
        key = event.key()
        """
        if key == QtCore.Qt.Key_Up:
            self.centerNode.moveBy(0, -20)
        elif key == QtCore.Qt.Key_Down:
            self.centerNode.moveBy(0, 20)
        elif key == QtCore.Qt.Key_Left:
            self.centerNode.moveBy(-20, 0)
        elif key == QtCore.Qt.Key_Right:
            self.centerNode.moveBy(20, 0)
        elif key == QtCore.Qt.Key_Plus:
            self.scaleView(1.2)
        elif key == QtCore.Qt.Key_Minus:
            self.scaleView(1 / 1.2)
        elif key == QtCore.Qt.Key_Space or key == QtCore.Qt.Key_Enter:
            for item in self.scene().items():
                if isinstance(item, nodes.Node):
                    item.setPos(-150 + QtCore.qrand() % 300, -150 + QtCore.qrand() % 300)
        else:"""
        super(NodeGraph, self).keyPressEvent(event)

    def mousePressEvent(self, event):
        self.update()

        #if event.buttons() == QtCore.Qt.LeftButton:
        #    print "lft"
        #elif event.buttons() == QtCore.Qt.RightButton:
        #    print "rght"
            #show instance of menu

        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        #if click out connection then set the node to connect to
        if isinstance(selectedNode, OutConnection):
            self.setDragMode(QtGui.QGraphicsView.NoDrag)
            self.nodeToConnectTo = selectedNode.parent
            #start temp line here
            self.drawTempLine = True
        else:
            self.nodeToConnectTo = None


        super(NodeGraph, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.update()
        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, Node):
            #remove any edges first
            selectedNode.setSelected(True)
            #print selectedNode.isSelected()
        super(NodeGraph, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        self.drawTempLine = False
        self.setDragMode(QtGui.QGraphicsView.ScrollHandDrag)
        #print event.globalPos()
        #event.button
        #event.buttonDownScenePos
        #event.buttonDownScreenPos
        #event.pos
        #event.buttonDownPos

        #update the size/bounds of the scene
        tw = 0
        th = 0
        for n in self.allNodes:
            tw += n.boundingRect().width()
            th += n.boundingRect().height()

        self.sRect.setX(-tw * 2)
        self.sRect.setY(-th * 3)
        self.sRect.setWidth(tw * 4)
        self.sRect.setHeight(th * 6)
        self.mainScene.setSceneRect(self.sRect)

        #print self.scene().itemAt(self.mapToScene(event.pos()))
        selectedNode = self.scene().itemAt(self.mapToScene(event.pos()))
        if isinstance(selectedNode, Node):
            self.selectedNode = selectedNode
            self.parent().parent().parent().displayNodeDetails()

        #if node is in connection then finish connection with node to connect to
        if isinstance(selectedNode, InConnection):
            #start in connection drawing
            if not self.nodeToConnectTo == None:
                con = self.nodeToConnectTo.outConnections[0]
                newEdge = Edge(con,
                                     selectedNode.parent.inputCon)
                selectedNode.parent.addEdge(newEdge)
                self.mainScene.addItem(newEdge)


        super(NodeGraph, self).mouseReleaseEvent(event)

    def timerEvent(self, event):
        nodess = [item for item in self.scene().items() if isinstance(item, Node)]

        itemsMoved = False
        for node in nodess:
            if node.advance():
                itemsMoved = True

        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0

    def wheelEvent(self, event):
        """
        zoom in and out of the graph
        """
        self.scaleView(math.pow(2.0, event.delta() / 240.0))

    def render(self, painter, option, widget):
        """
        """
        if self.drawTempLine:
            print "drawing"
            line = QtCore.QLineF(self.nodeToConnectTo.outputCon.pos(), self.pos())
            painter.setPen(QtGui.QPen(QtCore.Qt.white,
                                      1,
                                      QtCore.Qt.SolidLine,
                                      QtCore.Qt.RoundCap,
                                      QtCore.Qt.RoundJoin))
            painter.drawLine(line)
            self.mainScene.addItem(line)

    def drawBackground(self, painter, rect):
        # Shadow.
        """
        sceneRect = self.sceneRect()
        rightShadow = QtCore.QRectF(sceneRect.right(), sceneRect.top() + 5, 5,
                sceneRect.height())
        bottomShadow = QtCore.QRectF(sceneRect.left() + 5, sceneRect.bottom(),
                sceneRect.width(), 5)
        if rightShadow.intersects(rect) or rightShadow.contains(rect):
            painter.fillRect(rightShadow, QtCore.Qt.darkGray)
        if bottomShadow.intersects(rect) or bottomShadow.contains(rect):
            painter.fillRect(bottomShadow, QtCore.Qt.darkGray)"""

        # Fill.
        """
        gradient = QtGui.QLinearGradient(sceneRect.topLeft(),
                sceneRect.bottomRight())
        gradient.setColorAt(0, QtCore.Qt.white)
        gradient.setColorAt(1, QtCore.Qt.lightGray)
        painter.fillRect(rect.intersect(sceneRect), QtGui.QBrush(gradient))
        painter.setBrush(QtCore.Qt.NoBrush)
        painter.drawRect(sceneRect)"""

        # Text.
        #textRect = QtCore.QRectF(sceneRect.left() + 4, sceneRect.top() + 4,
        #        sceneRect.width() - 4, sceneRect.height() - 4)
        #message = "Click and drag the nodes around, and zoom with the " \
        #        "mouse wheel or the '+' and '-' keys"

        """
        font = painter.font()
        font.setBold(True)
        font.setPointSize(14)
        painter.setFont(font)
        painter.setPen(QtCore.Qt.lightGray)
        painter.drawText(textRect.translated(2, 2), message)
        painter.setPen(QtCore.Qt.black)
        painter.drawText(textRect, message)"""
        fillCol = QtGui.QColor()
        fillCol.setRgb(18, 19, 20, alpha=255)
        painter.setBrush(fillCol)
        painter.drawRect(self.sceneRect())


    def scaleView(self, scaleFactor):
        """
        scale the graph view up and down
        """
        factor = self.matrix().scale(scaleFactor, scaleFactor).mapRect(QtCore.QRectF(0, 0, 1, 1)).width()
        if factor < 0.07 or factor > 25:
            return
        self.scale(scaleFactor, scaleFactor)
        

#============================ NODEs===========================================#



class DragNode(QtGui.QWidget):
    """
    Basic Draggable node
    """
    
    def __init__(self, parent=None, details=None):
        """
        """
        super(DragNode, self).__init__()
        
        

class Node(QtGui.QGraphicsItem):
    """
    The basic node class
    """
    Type = QtGui.QGraphicsItem.UserType + 1
    

    def __init__(self, parent=None, details=None):
        super(Node, self).__init__()

        self.graph = parent
        self.nodeType = utils.enum('START',
                                   'TEXT',
                                   'BRANCH',
                                   'DELAY',
                                   'EVENT',
                                   'END')
        self.type = self.nodeType.START
        self.edgeList = []
        self.newPos = QtCore.QPointF()
        self.wh = (320,150)
        self.sz = {"halfw":self.wh[0]/2,
                   "halfh":self.wh[1]/2,
                   "fourw":self.wh[0]/4,
                   "fourh":self.wh[1]/4,
                   "fivew":self.wh[0]/5,
                   "fiveh":self.wh[1]/5,
                   "tenw":self.wh[0]/10,
                   "tenh":self.wh[1]/10}

        self.details = details
        #for k,v in details.iteritems():
        #    print k,v
        
        self.boundSize = QtCore.QRectF(0,-18,self.wh[0],self.wh[1]+30)
        self.shapeSize = QtCore.QRectF(0,0, self.wh[0],self.wh[1])
        self.rimColor = QtGui.QColor(55, 55, 55, alpha=200)
        self.id = details['id']
        
            
        self.inputIds = []
        for cid in details['in']:
            self.inputIds.append(cid)
        
        self.inConnections = []
        self.outConnections = []
        
        self.inputCon = InConnection(self)
        self.inputCon.setPos(0, 50)
        self.inConnections.append(self.inputCon)
        
        #make as many outputs as the node has, stacked vertically
        for i in range(len(details['out'])):
            self.addOutput(i)
        
        
        self.group = QtGui.QGraphicsItemGroup(scene=parent.scene())
        #self.group.addToGroup(self)
        self.group.addToGroup(self.inputCon)
        for g in self.outConnections:
            self.group.addToGroup(g)
            
        
        self.tempEdge = None
        self.tempin = None
        self.tempout = None
        
        self.setAcceptsHoverEvents(True)
        self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtGui.QGraphicsItem.ItemIsSelectable)
        self.setFlag(QtGui.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtGui.QGraphicsItem.DeviceCoordinateCache)
        self.setZValue(1)
        
        #print (details["position"][0])
        self.setPos(details["position"][0], details["position"][1])
        #print self.pos().x()
        self.refreshMe()

    def refreshMe(self):
        """
        refresh and update the details
        """
        if "portrait" in self.details:
            imgPth = self.details["portrait"]
            self.img = QtGui.QImage(imgPth)
        self.update()
        

    def type(self):
        return Node.Type

    def addEdge(self, edge):
        self.edgeList.append(edge)
        edge.adjust()
        
    def addOutput(self, i, new=False):
        """
        add another output connection to the node and a response for it
        """
        if new:
            print "creating new output"
            print self.details["responses"]
            self.details['responses'].append("new response")
            self.details['out'].append(0)
            print self.details["responses"]
            i = len(self.outConnections)
        oCon = OutConnection(self.details['out'][i], self)
        padd = 3
        h = i * oCon.boundingRect().height() + padd
        oCon.setPos(self.shapeSize.width(),
                    (self.shapeSize.height()/2) + h)
        self.outConnections.append(oCon)
        
    def removeEdge(self, edge):
        self.graph.mainScene.removeItem(edge)
        self.edgeList.remove(edge)
        

    def edges(self):
        return self.edgeList
    
    def closeMe(self):
        for e in self.edges():
            self.graph.mainScene.removeItem(e)
        self.graph.mainScene.removeItem(self.group)
        self.graph.mainScene.removeItem(self)

    def advance(self):
        if self.newPos == self.pos():
            return False

        self.setPos(self.newPos)
        return True

    def boundingRect(self):
        return self.boundSize
    
    def shape(self):
        path = QtGui.QPainterPath()
        path.addRoundedRect(self.shapeSize.x(),
                            self.shapeSize.y(),
                            self.shapeSize.width(),
                            self.shapeSize.height(),
                            7,
                            7)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor(55, 55, 55, alpha=200)
        painter.setBrush(fillCol)
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                7,
                                7)

        #draw outline
        #painter.setBrush(QtGui.QBrush(gradient))
        #fillCol = QtGui.QColor()
        #fillCol.setRgb(34, 35, 38, alpha=125)
        #painter.setBrush(fillCol)
        borderThickness = 3
        if self.isSelected():
            painter.setPen(QtGui.QPen(QtCore.Qt.white, borderThickness))
        else:
            painter.setPen(QtGui.QPen(self.rimColor, borderThickness))
        roundness = 7
        painter.drawRoundedRect(0,
                                0,
                                self.shapeSize.width(),
                                self.shapeSize.height(),
                                roundness,
                                roundness)
        
        #draw type
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        painter.setFont(QtGui.QFont('Roboto', 12))
        trec = QtCore.QRect(self.sz["tenh"],
                            -self.sz["tenh"] - 5,
                            self.wh[0],
                            24)
        painter.drawText(trec,
                         QtCore.Qt.AlignLeft,
                         "Type: {0}".format(self.details['type']) )
        
        #draw image
        imgwh = 64
        imgr = QtCore.QRect(self.sz["tenh"],
                            self.sz["tenh"],
                            imgwh,
                            imgwh)
        painter.drawImage(imgr, self.img)
        
        #draw name
        nrec = QtCore.QRect((self.sz["tenh"] *2) + imgwh,
                            self.sz["tenh"],
                            self.wh[0],
                            24)
        painter.drawText(nrec,
                         QtCore.Qt.AlignLeft,
                         "Who: {0}".format(self.details['who']) )
        
        #draw text
        opt = QtGui.QTextOption()
        opt.setAlignment(QtCore.Qt.AlignLeft)
        opt.setWrapMode(QtGui.QTextOption.WordWrap)
        painter.setPen(QtGui.QColor(225,225,225, alpha=255))
        trec = QtCore.QRectF(self.sz["tenh"],
                           self.sz["tenh"] + imgr.height(),
                           self.wh[0] - self.sz["fivew"],
                           self.sz["halfh"])
        painter.drawText(trec,
                         self.details['text'],
                         option = opt)
        
        #draw response
        i=0
        totalRespH = 0;
        for con in self.outConnections:
            nrec = QtCore.QRect((self.sz["tenh"] *2) + imgwh,
                                trec.height() + totalRespH,
                                self.wh[0],
                                24)
            totalRespH += 24
            painter.drawText(nrec,
                             QtCore.Qt.AlignLeft,
                             "Response: {0}".format(self.details['responses'][i]) )
            i+=1
        
        
        sw = trec.width()
        if (nrec.width() + imgr.width()) > trec.width():
            sw = (nrec.width() + imgr.width())
        sh = imgr.height() + trec.height() + totalRespH
        self.shapeSize.setWidth(sw + self.sz["tenh"])
        self.boundSize.setWidth(self.shapeSize.width() + self.sz["tenh"])
        self.shapeSize.setHeight(sh + self.sz["tenh"])
        self.boundSize.setHeight(self.shapeSize.height() + self.sz["tenh"])
        
       
        

    def itemChange(self, change, value):
        """
        update all the positions of the graphics
        """
        if change == QtGui.QGraphicsItem.ItemPositionHasChanged:
            for edge in self.edgeList:
                edge.adjust()
            self.graph.itemMoved()
            
        #update the position of the buttons and connections
        self.newPos = self.pos()
        self.inputCon.setPos(self.newPos.x() - self.inputCon.boundingRect().width(), 
                                 self.newPos.y() + (self.shapeSize.height()/2))
        
        padd = 3
        for i in range(len(self.outConnections)):
            h = (i * self.outConnections[i].boundingRect().height()) + padd
            self.outConnections[i].setPos(self.newPos.x() + self.shapeSize.width(),
                       self.newPos.y() + (self.shapeSize.height()/2) + h)

        return super(Node, self).itemChange(change, value)

    def mousePressEvent(self, event):
        self.update()
        super(Node, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.update()
        super(Node, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        #print event.pos()
        #print self.scene().itemAt(event.pos())
        super(Node, self).mouseReleaseEvent(event)

    def hoverEnterEvent(self, event):
        self.update()
        #set as current node
        self.graph.currentNode = self
        super(Node, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.update()
        super(Node, self).hoverLeaveEvent(event)
        
        
        
#================ NODE UI ELEMENTS ============================================#
        
class SimpleButton(QtGui.QGraphicsItem):
    
    def __init__(self, parent=None, details=None):
        super(SimpleButton, self).__init__()
        self.parent = parent
        self.fillColor = {"r":55, "g": 85, "b":155, "a":255}
        
    def boundingRect(self):
        return QtCore.QRectF(0,0,15,15)
    
    def shape(self):
        path = QtGui.QPainterPath()
        path.addEllipse(0, 0, 15, 15)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        
        fillCol = QtGui.QColor()
        fillCol.setRgb(self.fillColor["r"],
                       self.fillColor["g"],
                       self.fillColor["b"],
                       alpha=self.fillColor["a"])
        painter.setBrush(fillCol)
        painter.drawEllipse(0, 0, 15, 15)

        #draw outline
        fillCol = QtGui.QColor()
        fillCol.setRgb(55, 55, 55, alpha=125)
        painter.setBrush(fillCol)
        painter.setPen(QtGui.QPen(QtCore.Qt.white, 1))
        painter.drawEllipse(0, 0, 15, 15)
        
        
class InConnection(SimpleButton):
    
    def __init__(self, parent=None, details=None):
        super(InConnection, self).__init__()

        self.parent = parent
        self.graph = parent.graph
        self.fillColor = {"r":155, "g":155, "b":155, "a":125}
        self.graph.scene().addItem(self)
        
        
    def mousePressEvent(self, event):
        self.update()
        self.parent.tempin = self
        #print "set in as temp"
        super(InConnection, self).mousePressEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.update()
        super(InConnection, self).mouseDoubleClickEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        #print event.pos()
        #print self.scene().itemAt(event.pos())
        
        #clear any other connections if one exists
        if not self.parent.tempout.noodle == None:
            self.parent.removeEdge(self.parent.tempout.noodle)
            self.parent.tempout.noodle = None
            
        newEdge = Edge(self, self.parent.tempout)
        self.parent.tempout.noodle = newEdge
        self.parent.addEdge(newEdge)
        
        super(InConnection, self).mouseReleaseEvent(event)

    def hoverEnterEvent(self, event):
        self.update()
        super(InConnection, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.update()
        super(InConnection, self).hoverLeaveEvent(event)
        
        
        
class OutConnection(SimpleButton):
    
    def __init__(self, id, parent=None, details=None):
        super(OutConnection, self).__init__()

        self.parent = parent
        self.graph = parent.graph
        self.noodle = None;
        self.id = id
        self.graph.scene().addItem(self)
        
    def mousePressEvent(self, event):
        self.update()
        self.parent.tempout = self
        print "this is out {0}".format(self.parent.id)
        super(OutConnection, self).mousePressEvent(event)


        
class Edge(QtGui.QGraphicsItem):
    """
    The line that connects any 2 nodes
    """
    Pi = math.pi
    TwoPi = 2.0 * Pi

    Type = QtGui.QGraphicsItem.UserType + 2

    def __init__(self, sourceNode, destNode):
        super(Edge, self).__init__()

        self.arrowSize = 10.0
        self.lineThickness = 2.0;
        self.outputNodeWidth = 8;
        self.sourcePoint = QtCore.QPointF()
        self.destPoint = QtCore.QPointF()

        self.setAcceptedMouseButtons(QtCore.Qt.NoButton)
        self.source = sourceNode
        self.dest = destNode
        self.source.parent.addEdge(self)
        self.dest.parent.addEdge(self)
        self.adjust()

    def type(self):
        return Edge.Type

    def sourceNode(self):
        return self.source

    def setSourceNode(self, node):
        self.source = node
        self.adjust()

    def destNode(self):
        return self.dest

    def setDestNode(self, node):
        self.dest = node
        self.adjust()

    def adjust(self):
        if not self.source or not self.dest:
            return

        line = QtCore.QLineF(self.mapFromItem(self.source, 0, 0),
                self.mapFromItem(self.dest, 0, 0))
        length = line.length()

        self.prepareGeometryChange()

        if length > 20.0:
            edgeOffset = QtCore.QPointF((line.dx() * 10) / length,
                    (line.dy() * 10) / length)

            self.sourcePoint = line.p1() + edgeOffset
            self.destPoint = line.p2() - edgeOffset
        else:
            self.sourcePoint = line.p1()
            self.destPoint = line.p1()

    def boundingRect(self):
        if not self.source or not self.dest:
            return QtCore.QRectF()

        penWidth = 1.0
        extra = (penWidth + self.arrowSize) / 2.0

        return QtCore.QRectF(self.sourcePoint,
                QtCore.QSizeF(self.destPoint.x() - self.sourcePoint.x(),
                        self.destPoint.y() - self.sourcePoint.y())).normalized().adjusted(-extra, -extra, extra, extra)
                        

    def paint(self, painter, option, widget):
        if not self.source or not self.dest:
            return

        # Draw the line itself.
        line = QtCore.QLineF(self.sourcePoint, self.destPoint)
        if line.length() < 125:
            px1 = self.sourcePoint.x() + self.outputNodeWidth
            py1 = self.sourcePoint.y() + self.outputNodeWidth
            pt1 = QtCore.QPointF(px1, py1)
            px2 = self.destPoint.x() + self.outputNodeWidth
            py2 = self.destPoint.y() + self.outputNodeWidth
            pt2 = QtCore.QPointF(px2, py2)
            line = QtCore.QLineF(pt1, pt2)
            painter.setPen(QtGui.QPen(QtCore.Qt.white,
                                      self.lineThickness,
                                      QtCore.Qt.SolidLine,
                                      QtCore.Qt.RoundCap,
                                      QtCore.Qt.RoundJoin))
            painter.drawLine(line)
        else:
            line = self.doBezier(painter)

        #if line.length() == 0.0:
        #    return

        #painter.setPen(QtGui.QPen(QtCore.Qt.white, 1, QtCore.Qt.SolidLine,
        #        QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        #painter.drawLine(line)

        # Draw the arrows if there's enough room.
        
        """
        angle = math.acos(line.dx() / line.length())
        if line.dy() >= 0:
            angle = Edge.TwoPi - angle

        sourceArrowP1 = self.sourcePoint + QtCore.QPointF(math.sin(angle + Edge.Pi / 3) * self.arrowSize,
                                                          math.cos(angle + Edge.Pi / 3) * self.arrowSize)
        sourceArrowP2 = self.sourcePoint + QtCore.QPointF(math.sin(angle + Edge.Pi - Edge.Pi / 3) * self.arrowSize,
                                                          math.cos(angle + Edge.Pi - Edge.Pi / 3) * self.arrowSize);
        destArrowP1 = self.destPoint + QtCore.QPointF(math.sin(angle - Edge.Pi / 3) * self.arrowSize,
                                                      math.cos(angle - Edge.Pi / 3) * self.arrowSize)
        destArrowP2 = self.destPoint + QtCore.QPointF(math.sin(angle - Edge.Pi + Edge.Pi / 3) * self.arrowSize,
                                                      math.cos(angle - Edge.Pi + Edge.Pi / 3) * self.arrowSize)

        painter.setBrush(QtCore.Qt.black)
        painter.drawPolygon(QtGui.QPolygonF([line.p1(), sourceArrowP1, sourceArrowP2]))
        painter.drawPolygon(QtGui.QPolygonF([line.p2(), destArrowP1, destArrowP2]))
        """
        
    def doBezier(self, qp):
        """
        """
        #blackPen = QtGui.QPen(QtCore.Qt.black, 1, QtCore.Qt.DashLine)
        #redPen = QtGui.QPen(QtCore.Qt.red, 1, QtCore.Qt.DashLine)
        #bluePen = QtGui.QPen(QtCore.Qt.blue, 1, QtCore.Qt.DashLine)
        #greenPen = QtGui.QPen(QtCore.Qt.green, 1, QtCore.Qt.DashLine)
        #redBrush = QtGui.QBrush(QtCore.Qt.red)
        whitePen = QtGui.QPen(QtCore.Qt.white,
                              self.lineThickness,
                              QtCore.Qt.SolidLine)
 
        steps = 100
        controlPoints = (
            (self.sourcePoint.x() + self.outputNodeWidth, self.sourcePoint.y() + self.outputNodeWidth), 
            (self.sourcePoint.x() + 100, self.sourcePoint.y() + self.outputNodeWidth), 
            (self.destPoint.x() - 100, self.destPoint.y() + self.outputNodeWidth), 
            (self.destPoint.x() + self.outputNodeWidth, self.destPoint.y() + self.outputNodeWidth))
        oldPoint = controlPoints[0]
        
        """
        Vector3 startPos = new Vector3(start.x + start.width, start.y + start.height / 2, 0);
        Vector3 endPos = new Vector3(end.x, end.y + end.height / 2, 0);
        Vector3 startTan = startPos + Vector3.right * 50;
        Vector3 endTan = endPos + Vector3.left * 50;

        Handles.DrawBezier(startPos, endPos, startTan, endTan, Color.white, null, 2);
        """

        lns = []
        
        qp.setPen(whitePen)
        for point in utils.bezier_curve_range(steps, controlPoints):
            ln = qp.drawLine(oldPoint[0], oldPoint[1], point[0], point[1])
            lns.append(ln)
            oldPoint = point
        return lns[0]
        

        
        
        
        
        
        
        
        
        
        
        
        