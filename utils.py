'''
Created on Jul 18, 2014

@author: Matt
'''

from PyQt4 import QtGui
from PyQt4 import QtCore
import ConfigParser
import os
import sys
import logging
import json
import math

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

def dist(x1, y1, x2, y2):
    """
    find the distance between 2 points as vec2d
    """
    xd = x1-x2
    yd = y1-y2
    return math.sqrt((xd*xd) + (yd*yd))

def get_node_by_id(nodes, id):
    """
    search all the scene nodes and return the one with the input id
    @param nodes: a list of all the nodes in the scene
    @param id: an int that represents the id of the node being searched for
    @return: a Node if found in the list else None
    """
    for n in nodes:
        if n.id == id:
            return n
    
    return None

def get_root_url():
    """
    return the base path to these modules
    """
    return os.path.dirname(os.path.abspath('__file__'))

def get_stylesheet():
    """
    Load the stylesheet for the application
    
    @return dat: the contents of the stylesheet
    """
    f=open(os.path.join(get_root_url(),'dialogue.stylesheet'), 'r')
    dat = f.read()
    f.close()
    return dat

def generic_msg(msg, msg_type, dialog=False):
    """
    generic_msg is a helper function to alert the user to either an
    information dialog or an info dialog
    
    @type msg: str
    @param msg: The message to be displayed
    @type msg_type: str
    @param msg_type: The message type. Valid types are info, warning and error
    @type dialog: bool
    @param dialog: Display a dialog or not. Default is False
     
    """
    # set as lower case in case the type has a caps letter in it
    msg_type = str.lower(msg_type)
    if msg_type == 'error' or msg_type == 'warning':
        sys.stderr.write('%s\n' % msg)
    else:
        sys.stdout.write('%s\n' % msg)
    
    res = None
    if dialog:
        if msg_type == 'error':
            res = QtGui.QMessageBox.critical(None, 'Error', str(msg))
        elif msg_type == 'warning':
            res = QtGui.QMessageBox.warning(None, 'Warning', str(msg))
        elif msg_type == 'info':
            res = QtGui.QMessageBox.information(None, 'Information', str(msg))
        elif msg_type == 'quest':
            res = QtGui.QMessageBox.question(None, 'Question', str(msg))
    return res

def getDialogueData(cfg):
    """
    load the json data
    @param: cfg - an instance of the userconfig
    """
    try:
        cfg.settings["current_file_opened"] = cfg.settings["last_file_opened"]
        print "opening "+ cfg.settings["last_file_opened"]
        with open(cfg.settings["last_file_opened"], 'r') as dat:
            jData = json.loads(dat.read())
        return jData
    except Exception,e:
        print e.message
        return False
            

def setDialogueData(jData, cfg):
    """
    save the current dialog scenes
    @param jData: a dict containing all data for all scenes
    @para cfg: an instance of the user config 
    """
    try:
        with open(cfg.settings["current_file_opened"], 'w') as dat:
            dat.write(json.dumps(jData))
    except Exception,e:
        print e.message
        return False
    
    m = generic_msg("Save Successful", "info", True)
    return True
    
def defaultDataStructs(type, kwargs=None):
    """
    return the default data structure for new node based on type
    @param type: a string representing the type of node
    """
    newDetails = None
    if type == "start":
        newDetails = {"id":0,
                      "type":"start",
                      "text":"startNode",
                      "responses":"continue",
                      "portrait":"",
                      "audio":[""],
                      "position":[0,0],
                      "who":"none",
                      "in":[0],
                      "out":[0]}
    elif type == "text":
        newDetails = {"id":0,
                      "type":"text",
                      "text":"some simple sentence",
                      "responses":["next"],
                      "portrait":"",
                      "audio":[""],
                      "position":[175,0],
                      "who":"none",
                      "in":[0],
                      "out":[0]}
    elif type == "branch":
        newDetails = {"id":0,
                      "type":"branch",
                      "text":"some simple sentence",
                      "responses":["next"],
                      "portrait":"",
                      "audio":[""],
                      "position":[0,0],
                      "who":"none",
                      "in":[0],
                      "out":[0]}
    elif type == "delay":
        newDetails = {"id":0,
                      "type":"delay",
                      "text":"some simple sentence",
                      "time":1,
                      "position":[0,0],
                      "in":[0],
                      "out":[0]}
    elif type == "event":
        newDetails = {"id":0,
                      "type":"event",
                      "method":"noMethod",
                      "methodArgs":{"name":"value"},
                      "position":[0,0],
                      "in":[0],
                      "out":[0]}
    elif type == "end":
        newDetails = {"id":0,
                      "type":"end",
                      "text":"the end",
                      "audio":[""],
                      "in":[0],
                      "out":[0]}
    elif type == "dialogue":
        newDetails = {"id":kwargs["listlen"],
                       "name":"{0} dialogue".format(kwargs["listlen"]),
                       "who":"new",
                       "tree":[defaultDataStructs('start')]
                       }
        
    return newDetails
    

class UserConfig():
    """
    This class will hold all the commands for the current user
    An instance will be created on load, and the data will be stored
    and manipulated in this class then on exit, the data will be saved
    from this class
    """
    
    def __init__(self, rootDir=None):
        """
        init
        """
        rootDir = get_root_url()
        self.configFileName = os.path.join(rootDir, "settings.ini")
        self.settings = {}
        
        #check and see if config has been created, create if not
        if os.path.exists(self.configFileName):
            self.readConfig()
        else:
            self.createConfig()
        
        
    def createConfig(self):
        """
        create the initial config file if doesn't exist
        """
        logging.info("No Config File found, creating new one: {0}".format(self.configFileName))
        config = ConfigParser.ConfigParser()
        config.add_section("Settings")
        
        #general settings
        config.set("Settings", "last_file_opened", "G:\\GameDevelopment\\Unity\\Dialog System\\Assets\\ds_data.json")
        config.set("Settings", "last_dir_opened", "G:\\GameDevelopment\\Unity\\Dialog System\\Assets\\")
        config.set("Settings", "current_opened_file", "G:\\GameDevelopment\\Unity\\Dialog System\\Assets\\ds_data.json")
        
        #save file
        with open(self.configFileName, 'w') as cFile:
            config.write(cFile)
        
        #now the file is created run the readConfig function to set defaults
        self.readConfig()
    
    def readConfig(self):
        """
        read the config file and set settings
        """
        logging.info("Reading config file: {0}".format(self.configFileName))
        #open config
        config = ConfigParser.ConfigParser()
        config.read(self.configFileName)
        
        print(config.items("Settings"))
        
        #set config items to settings dict
        self.settings = {}
        
        for set in ["Settings"]:
            for (k,v) in config.items(set):
                self.settings[k] = v
            
        logging.info("read settings are: {0}".format(self.settings))


def binomial(i, n):
    """Binomial coefficient"""
    return math.factorial(n) / float(
        math.factorial(i) * math.factorial(n - i))
 
 
def bernstein(t, i, n):
    """Bernstein polynom"""
    return binomial(i, n) * (t ** i) * ((1 - t) ** (n - i))
 
 
def bezier(t, points):
    """Calculate coordinate of a point in the bezier curve"""
    n = len(points) - 1
    x = y = 0
    for i, pos in enumerate(points):
        bern = bernstein(t, i, n)
        x += pos[0] * bern
        y += pos[1] * bern
    return x, y
 
 
def bezier_curve_range(n, points):
    """Range of points in a curve bezier"""
    for i in xrange(n):
        t = i / float(n - 1)
        yield bezier(t, points)

