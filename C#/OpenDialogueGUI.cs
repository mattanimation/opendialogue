﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Net;

public class OpenDialogueGUI : MonoBehaviour {

	/*
	 * THis class is an example of how the dialogue system would be used
	 * in conjunction with whatever gui system is being used
	 * */


	private bool _showing;
	private string _text;
	private string _who;
	private string _img;
	private TextAsset imgAsset;
	private Texture2D imgTex;
	private Texture guiTex;
	private List<string> _responses;
	private bool conversing =false;

	// Use this for initialization
	void Start () {

		//need to init here and pass a ref of
		//mono class to main class so can do coroutines
		OpenDialogue.Initialize(this);
	
		//add callbacks to event listeners
		//NEED TO FIGURE THIS OUT!!!!!!!!!!!!!!
		OpenDialogue.onStarted += onStarted;
		OpenDialogue.onEnded += onEnded;
		OpenDialogue.onTextPhase += onTextPhase;
		OpenDialogue.onCustomMethod += onCustomMethod;
	}
	
	void OnGUI()
	{
		if(!_showing)
			return;


		GUI.Box(new Rect(10,10, 200, 150), "conversation with "+ _who);
		GUI.Label(new Rect(10, 25, 190, 140), _text);
	
		GUI.DrawTexture(new Rect(10,50, 128,128), guiTex);

		for(int i=0; i < _responses.Count; i++)
		{
			if(GUI.Button(new Rect(10, 220 + (i * 45), 200, 30), _responses[i]))
			{
				OpenDialogue.ContinueDialogue(i);
			}
		}
		

	}

	/**
	 * local implementation of what to do when dialogue starts
	 * */
	private void onStarted()
	{
		_showing = true;
	}

	/**
	 * local implementation of what to do when dialogue ends
	 * */
	private void onEnded()
	{
		_showing = false;
	}

	/**
	 * local implemenation of what custom method to fire, this
	 * can be changed to load methods from any other classes not just
	 * this one, you would just need to change the variable "od" to some
	 * other class and make sure the method you are calling is public
	 * */
	private void onCustomMethod(string methName)
	{
		OpenDialogueGUI od = new OpenDialogueGUI();
		Type thisType = od.GetType();
		MethodInfo theMeth = thisType.GetMethod(methName);

		if(theMeth != null)
		{
			//setup a way to pass arguments
			object[] param = new object[]{5,10};
			theMeth.Invoke(od, param);
		}
	}

	/*
	 * the custom method as listed in the JSON file from open dialogue
	 * make sure and tell the dialogue when this finished
	 * */
	public void customMethod(int arg1, int arg2)
	{
		Debug.Log ("WOAH THE EVENT FIRED, args: " + arg1 + "  " + arg2);
		//update dialogue to continue
		OpenDialogue.CustomMethodComplete();
	}


	/**
	 * local implementation of what to do when a dialouge is updated
	 * other methods could be added here if animations or other events
	 * need to transpire on each update of data
	 * */
	private void onTextPhase(OpenDialogueData data)
	{
		//this method updates the data currently being displayed in the GUI
		_text = data.text;
		_responses = data.responses;
		_who = data.who;
		_img = data.portrait;
		StartCoroutine("loadImage");
	}

	IEnumerator loadImage()
	{

		WWW www = new WWW(_img);
		yield return www;

		//imgTex = new Texture2D(128,128,TextureFormat.ARGB32, false);
		//www.LoadImageIntoTexture(imgTex);
		//guiTex = imgTex as Texture;
	}

}
