﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using System.Text;

public class OpenDialogueData  {

	/*
	 * This is the model class that all data from the node editor will be represnted by
	 * when json data is loaded it will get converted to and instance in this format
	 * */

	public enum NodeType{
		START,
		TEXT,
		BRANCH,
		DELAY,
		EVENT,
		END
	};

	public OpenDialogueData(JSONNode node)
	{
		_id = node["id"];
		_text = node["text"];
		_type = node["type"];

		//set time if there
		if(node["time"].AsBool);
			_time = node["time"].AsFloat;
		//set method if there
		if(node["method"].AsBool)
			_meth = node["method"];

		_inConnections = node["in"].AsArray;
		_outConnections = node["out"].AsArray;
		_who = node["who"].ToString();
		_portrait = node["portrait"].ToString();
		//this is used for the tool
		//_position = node["position"].AsArray;
		_audio = node["audio"].ToString();

		/*
		Debug.Log(_id);
		Debug.Log(_text);
		Debug.Log(_type);
		Debug.Log(_time);
		Debug.Log(_meth);
		Debug.Log(_inConnections);
		Debug.Log(_outConnections);*/

		_responses = new List<string>();
		//check over the connections and set choices
		//based on the ids of other nodes
		foreach(JSONNode resp in node["responses"].AsArray)
		{
			_responses.Add(resp.ToString());
		}
		//Debug.Log(_choices);

	}

	private string _id;
	public string id{
		get{return _id;}
		set{_id = value;}
	}

	private string _text;
	public string text{
		get{return _text;}
		set{_text = value;}
	}

	private float _time;
	public float time{
		get{return _time;}
		set{_time = value;}
	}

	private string _meth;
	public string meth{
		get{return _meth;}
		set{_meth = value;}
	}

	private string _who;
	public string who{
		get{return _who;}
		set{_who = value;}
	}

	private string _portrait;
	public string portrait{
		get{return _portrait;}
		set{_portrait = value;}
	}

	/*
	private JSONArray _position;
	public JSONArray position{
		get{return _position;}
		set{_position = value;}
	}*/

	private string _audio;
	public string audio{
		get{return _audio;}
		set{_audio = value;}
	}


	private string _response;
	public string response{
		get{return _response;}
		set{_response = value;}
	}
	
	private List<string> _responses;
	public List<string> responses{
		get{return _responses;}
		set{_responses = value;}
	}

	private string _type;
	public string type{
		get{return _type;}
		set{_type = value;}
	}

	private JSONArray _inConnections;
	public JSONArray inConnections{
		get{return _inConnections;}
		set{_inConnections = value;}
	}

	private JSONArray _outConnections;
	public JSONArray outConnections{
		get{return _outConnections;}
		set{_outConnections = value;}
	}


}
