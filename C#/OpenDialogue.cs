﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using SimpleJSON;
using System.Text;
using System.IO;

public class OpenDialogue {

	private static OpenDialogue instance;

	public static JSONNode dialogueData;
	public static string rootDir = "c:\\";
	public static string fileName = "ds_Data.json";
	public static bool isConversing = false;
	public static bool canContinue = true;

	private static Func<bool> endDialogueCallback;

	//use instance of class that extends mono to use coroutines
	public static OpenDialogueGUI gui;

	public static int currentDid = 0;
	public static int currentTid = 0;
	public static OpenDialogueData currentDataSet;

	/*
	 * Create the main instance of the class
	 * */
	public OpenDialogue()
	{
		if(instance != null)
			return;
		
		instance = this;
		
	}

	/*
	 * Set the initial instance of the class else return current instance
	 * */
	public static OpenDialogue Instance
	{
		get {
			if(instance == null)
				new OpenDialogue();
			
			return instance;
		}
	}


	public static void Initialize(OpenDialogueGUI odgui)
	{
		//start this puppy
		//events = new DialogueEvents();
		dialogueData = new JSONNode();
		OpenDialogue.LoadDialogueData();
		gui = odgui;
	}

	//========================EVENTS==================================================


	/*
	 * This event to fire when a dialogue starts
	 * */
	public delegate void OnStarted();
	public static event OnStarted onStarted;
	
	/*
	 * This event to fire when a dialogue ends
	 * */
	public delegate void OnEnded();
	public static event OnEnded onEnded;
	
	/*
	 * This event to fire when a dialgue selection is made and text is updated/changed
	 * it will have a DialogueSystemData type variable that is sent back 
	 * */
	public delegate void OnTextPhase(OpenDialogueData data);
	public static event OnTextPhase onTextPhase;

	/*
	 * This event will fire when an event node needs to
	 * complete a method determined by the users in the gui file
	 * */
	public delegate void OnCustomMethod(string methodName);
	public static event OnCustomMethod onCustomMethod;
	


	//=======================END EVENTS=================================================


	public static void StartDialogue(int dId)
	{
		//start a dialogue based on the passed in id
		Debug.Log ("start dialogue " + dId);
		Debug.Log(dialogueData["dialogues"][0]["tree"]);
	}

	public static void StartDialogue(int dId, Func<bool> callback)  //or Func<string> or Action<string> callback to pass argurment with callback function
	{
		//start a dialogue from id and set a callback function to fire
		Debug.Log ("start dialogue " + dId);
		//callback("asdf");
		currentDid = dId;
		currentDataSet = new OpenDialogueData(dialogueData["dialogues"][currentDid]["tree"][0]);
		//this call back shouldn't run until dialogue is finished
		endDialogueCallback = callback;
		//fire off the events to update the gui
		onTextPhase(currentDataSet);
		onStarted();
		isConversing = true;
		//callback();
	}

	public static void StartDialogue(string charName)
	{
		//find dataset for character with this name
		foreach(JSONNode node in dialogueData["dialogues"].AsArray)
		{
			//Debug.Log (node.ToString());
			//Debug.Log(charName);

			string who = node["who"].ToString().Trim().ToLower();
			bool match = (who.Contains(charName.Trim().ToLower()));
			//Debug.Log(match);
			if(match)
			{
				Debug.Log ("matched name!");
				currentDataSet = new OpenDialogueData(node["tree"][0]);
				break;
			}
		}

		onTextPhase(currentDataSet);
		onStarted();
		isConversing = true;
	}

	/*
	 * This method grabs the next connections id and passes it to 
	 * the loadNode method
	 * */
	public static void ContinueDialogue(int id)
	{
		if(canContinue)
		{
			int nxtItm = currentDataSet.outConnections[id].AsInt;
			LoadNode(nxtItm);
		}
	}

	/**
	 * This Method updates the current data set and then based on
	 * the node that will be loaded next, determines the next 
	 * course of action, from ending, dealying, running a custom method
	 * or just display the next set of information
	 * */
	private static void LoadNode(int id)
	{
		//Debug.Log("showing next node info: " +id);
		currentDataSet = new OpenDialogueData(dialogueData["dialogues"][currentDid]["tree"][id]);
		//Debug.Log (currentDataSet.type);
		switch(currentDataSet.type)
		{
			case "end":
				EndDialogue();
				break;
			case "delay":
				//Timer t = new Timer(currentDataSet.time);
				//t.Elapsed += OnTimedEvent;
				//t.Enabled = true;
			gui.StartCoroutine(DelayDialogue());
				canContinue = false;
				onTextPhase(currentDataSet);
				break;
			case "event":
				onCustomMethod(currentDataSet.meth);
				break;
			default:
				onTextPhase(currentDataSet);
				break;
		}

	}

	public static void CustomMethodComplete()
	{
		Debug.Log("CUSTOM METHOD ENDED!");
		LoadNode(currentDataSet.outConnections[0].AsInt);
	}

	private static IEnumerator DelayDialogue()
	{
		yield return new WaitForSeconds(currentDataSet.time);

		canContinue = true;
		//move on to next node
		Debug.Log(currentDataSet.outConnections);
		//currentDataSet = new OpenDialogueData(dialogueData["dialogues"][currentDid]["tree"][currentDataSet.outConnections[0]]);
		LoadNode(currentDataSet.outConnections[0].AsInt);
	}

	private static void OnTimedEvent(object source, ElapsedEventArgs e)
	{

		//continue to next panel
		currentDataSet = new OpenDialogueData(dialogueData["dialogues"][currentDid]["tree"][currentDataSet.outConnections[0]]);
		onTextPhase(currentDataSet);
		//Timer t = source as Timer;
		//t.Stop();
		//t.Enabled = false;
		//canContinue = true;
		Debug.Log("delay finished at " + e.SignalTime);

	}


	public static void EndDialogue()
	{
		//this end dialogue runs the callback that was
		//sent in on the start dialogue function
		if(endDialogueCallback != null)
			endDialogueCallback();
		onEnded();
		isConversing = false;
	}

	
	/*
	 * Load the whole dialog tree data
	 * */
	public static void LoadDialogueData()
	{
		rootDir = Application.dataPath;
		string fName = rootDir +"/"+ fileName;
		Debug.Log (fName);
		bool fileLoaded = Load(fName);
		Debug.Log(fileLoaded);
	}

	/*
	 * Load a file from a path
	 * */
	private static bool Load(string flName)
	{
		string ln = "";
		string allLines = "";
		try
		{
			//convert dData to string
			string dDataString = ""; // dialogueData.ToString();
			StreamReader sr = new StreamReader(flName, Encoding.Default);

			using(sr)
			{
				do
				{
					ln = sr.ReadLine();
					if(ln != null)
					{
						allLines += ln;
					}
				}
				while(ln != null);
			}

			sr.Close();
			//set data here
			dialogueData = JSON.Parse(allLines);
			Debug.Log ("initial data load");
			Debug.Log (dialogueData);
			return true;
			
		}
		catch(IOException e)
		{
			Debug.Log(e.Message);
			return false;
		}

	}

	/*
	 * Save the dialog state data
	 * */
	public void SaveDialogueData()
	{
		rootDir = Application.dataPath;
		string fName = rootDir + fileName;
		dialogueData.SaveToFile(fName);
		//bool fileSaved = Save (fName);
		//Debug.Log(fileSaved);
	}

	/*
	 * 
	 * Save a file to a path
	 * */
	private bool Save(string flName)
	{
		try
		{
			using(FileStream fs = File.Create(flName)){
				AddText(fs, "");
			}
				
			return true;
		}
		catch(IOException e)
		{
			Console.WriteLine("{0}\n", e.Message);
			return false;
		}
	}

	private static void AddText(FileStream fs, string value)
	{
		byte[] info = new UTF8Encoding(true).GetBytes(value);
		fs.Write(info, 0, info.Length);
	}

}
